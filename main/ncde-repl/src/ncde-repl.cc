/**
 * Copyright 2022 Lucerne Applied University of Sciences / IoT S&S Research Group
 *
 * Licensed under the Apache License, Version 2.0 (the "License");
 * you may not use this file except in compliance with the License.
 * You may obtain a copy of the License at
 * 
 *     http://www.apache.org/licenses/LICENSE-2.0
 *
 * Unless required by applicable law or agreed to in writing, software
 * distributed under the License is distributed on an "AS IS" BASIS,
 * WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
 * See the License for the specific language governing permissions and
 * limitations under the License.
 * 
 */

// https://github.com/nekipelov/httpparser/blob/master/src/httpparser/httprequestparser.h

#include <stdio.h>
#include <netinet/in.h>
#include <string.h>
#include <ctype.h>
#include <unistd.h>
#include <stdlib.h>
#include <signal.h>
#include <netdb.h>
#include <liburing.h>
#include <sys/stat.h>
#include <fcntl.h>
#include <getopt.h>
#include <string.h>
#include <queue>
#include <string>
#include <vector>
#include "httpparser/request.h"
#include "httpparser/httprequestparser.h"
#include "httpparser/response.h"
#include "httpparser/httpresponseparser.h"


#define QUEUE_DEPTH             256
#define READ_SZ                 8192

 // client connected
#define EVENT_TYPE_ACCEPT       0
// server accepeted connection -> issue read from client
#define EVENT_TYPE_CONNECT      1
 // client read -> issue write to server 
#define EVENT_TYPE_CLIENT_READ  2
 // done
#define EVENT_TYPE_CLIENT_WRITE 3
 // read from server -> issue write to client
#define EVENT_TYPE_SERVER_READ  4
 // server write -> issue write to client
#define EVENT_TYPE_SERVER_WRITE 5
 // server write -> issue write to client
#define EVENT_TYPE_CLOSE_FROM   6
 // server write -> issue write to client
#define EVENT_TYPE_CLOSE_TO     7

struct http_request_reponse {
    std::string request;
    std::string reponse;
};

struct options {
    int   listen_port;
    char* master_host;
    int   master_port;
    char* replica_host;
    int   replica_port;
    bool  verbose;
} options;

struct globals {
    int server_socket;
    struct addrinfo *master_addrinfo;
    struct addrinfo *server_addrinfo2;
    std::queue<struct http_request_reponse> requests;
} globals;

struct connection {
    int from_socket;
    int to_socket;
    bool closing;
    httpparser::Request request;
    unsigned char* request_raw;
    int request_raw_size;
    httpparser::Response response;
    unsigned char* response_raw;
    int response_raw_size;
};

struct request {
    struct connection* connection;
    int event_type;
    unsigned char* buf;
    int size;
    int res;
};

struct io_uring ring;

const char *usage_message = \
    "usage: %s <args>\r\n"
    "--listen-port=<port>\r\n"
    "--master-host=<hostname>\r\n"
    "--master-port=<port>\r\n"
    "--replica-host=<hostname>\r\n"
    "--replica-port=<port>\r\n"
    "\r\n";

// prints the system call and the error details exits with exit code 1 (error).
void fatal_error(const char *syscall) {
    perror(syscall);
    exit(1);
}

// helper function for cleaner looking code.
void *zh_malloc(size_t size) {
    void *buf = malloc(size);
    if (!buf) {
        fatal_error("Fatal error: unable to allocate memory.\n");
    }
    return buf;
}

// sets up the main listening socket used by the web server
void setup_listening_socket() {
    int sock = socket(AF_INET, SOCK_STREAM, 0);
    if (sock == -1) {
        fatal_error("socket()");
    }
    int enable = 1;
    if (setsockopt(sock, SOL_SOCKET, SO_REUSEADDR, &enable, sizeof(int)) < 0) {
        fatal_error("setsockopt(SO_REUSEADDR)");
    }

    struct sockaddr_in srv_addr;
    memset(&srv_addr, 0, sizeof(srv_addr));
    srv_addr.sin_family = AF_INET;
    srv_addr.sin_port = htons(options.listen_port);
    srv_addr.sin_addr.s_addr = htonl(INADDR_ANY);

    // bind to a port and turn this socket into a listening socket
    if (bind(sock, (const struct sockaddr*) &srv_addr, sizeof(srv_addr)) < 0) {
        fatal_error("bind()");
    }

    if (listen(sock, 10) < 0) {
        fatal_error("listen()");
    }
    globals.server_socket = sock;
}

struct request* make_request(struct connection* connection, int event_type, unsigned char* buf, int size) {
    struct request *req = (struct request*) malloc(sizeof(*req));
    req->connection = connection;
    req->event_type = event_type;
    req->buf = buf;
    req->size = size;
    return req;
}

int create_client_socket(struct addrinfo* ai) {
    int clientfd = socket(ai->ai_family, ai->ai_socktype, ai->ai_protocol);
    if (clientfd == -1) {
        fatal_error("create_client_socket: socket(AF_INET, SOCK_STREAM, 0)");
    }
    return clientfd;
}

struct connection* create_connection(int from_socket, struct addrinfo* ai) {
    struct connection* connection = (struct connection*) (malloc(sizeof (struct connection)));
    connection->from_socket = from_socket;
    connection->to_socket = create_client_socket(ai);
    connection->closing = false;
    return connection;
}

void add_accept_request() {
    if (options.verbose) printf("enqueue accept request\n");
    struct request *req = make_request(NULL, EVENT_TYPE_ACCEPT, (unsigned char*)malloc(READ_SZ), 0);
    struct io_uring_sqe *sqe = io_uring_get_sqe(&ring);
    io_uring_prep_accept(sqe, globals.server_socket, NULL, NULL, 0);
    io_uring_sqe_set_data(sqe, req);
    io_uring_submit(&ring);
}

void add_connect_request(struct connection* connection, struct sockaddr *client_addr, socklen_t client_addr_len) {
    if (options.verbose) printf("enqueue connection request\n");
    struct request *req = make_request(connection, EVENT_TYPE_CONNECT, NULL, 0);
    struct io_uring_sqe *sqe = io_uring_get_sqe(&ring);
    io_uring_prep_connect(sqe, connection->to_socket, client_addr, client_addr_len);    
    io_uring_sqe_set_data(sqe, req);
    io_uring_submit(&ring);
}

void add_read_from_request(struct connection* connection) {
    if (options.verbose) printf("enqueue read from request\n");
    struct request *req = make_request(connection, EVENT_TYPE_CLIENT_READ, (unsigned char*) malloc(READ_SZ), READ_SZ);
    struct io_uring_sqe *sqe = io_uring_get_sqe(&ring);
    io_uring_prep_read(sqe, connection->from_socket, req->buf, req->size, 0);
    io_uring_sqe_set_data(sqe, req);
    io_uring_submit(&ring);
}

void add_write_to_request(struct connection* connection, unsigned char* buf, int size) {
    if (options.verbose) printf("enqueue write to request\n");
    struct request *req = make_request(connection, EVENT_TYPE_SERVER_WRITE, buf, size);
    struct io_uring_sqe *sqe = io_uring_get_sqe(&ring);
    io_uring_prep_write(sqe, connection->to_socket, req->buf, size, 0);
    io_uring_sqe_set_data(sqe, req);
    io_uring_submit(&ring);
}

void add_read_to_request(struct connection* connection) {
    if (options.verbose) printf("enqueue read to request\n");
    struct request *req = make_request(connection, EVENT_TYPE_SERVER_READ, (unsigned char*) malloc(READ_SZ), READ_SZ);
    struct io_uring_sqe *sqe = io_uring_get_sqe(&ring);
    io_uring_prep_read(sqe, connection->to_socket, req->buf, req->size, 0);
    io_uring_sqe_set_data(sqe, req);
    io_uring_submit(&ring);
}

void add_write_from_request(struct connection* connection, unsigned char* buf, int size) {
    // TODO: if this was a write request and it was sucessful => enter into replication queue
    if (options.verbose) printf("enqueue write from request\n");
    struct request *req = make_request(connection, EVENT_TYPE_CLIENT_WRITE, buf, size);
    struct io_uring_sqe *sqe = io_uring_get_sqe(&ring);
    io_uring_prep_write(sqe, connection->from_socket, req->buf, size, 0);
    io_uring_sqe_set_data(sqe, req);
    io_uring_submit(&ring);
}

void add_close_from_request(struct connection* connection) {
    if (options.verbose) printf("enqueue close from request\n");
    struct request *req = make_request(connection, EVENT_TYPE_CLOSE_FROM, NULL, 0);
    struct io_uring_sqe *sqe = io_uring_get_sqe(&ring);
    io_uring_prep_close(sqe, connection->from_socket);
    io_uring_sqe_set_data(sqe, req);
    io_uring_submit(&ring);
}

void add_close_to_request(struct connection* connection) {
    if (options.verbose) printf("enqueue close to request\n");
    struct request *req = make_request(connection, EVENT_TYPE_CLOSE_TO, NULL, 0);
    struct io_uring_sqe *sqe = io_uring_get_sqe(&ring);
    io_uring_prep_close(sqe, connection->to_socket);
    io_uring_sqe_set_data(sqe, req);
    io_uring_submit(&ring);
}

void handle_accept_request(struct request* req) {
    if (options.verbose) printf("handle accept request\n");
    struct connection* connection = create_connection(req->res, globals.master_addrinfo);
    add_connect_request(connection, globals.master_addrinfo->ai_addr, globals.master_addrinfo->ai_addrlen);
    add_accept_request();
    free(req);
}

void handle_connect_request(struct request* req) {
    if (options.verbose) printf("handle connect request\n");
    add_read_from_request(req->connection);
    add_read_to_request(req->connection);
    free(req);
}

void handle_read_from_request(struct request* req) {
    if (options.verbose) printf("handle read from request\n");
    if (req->connection->closing) {
        free(req->buf);
        free(req);
        return;
    }
    if (req->res > 0) {
        httpparser::HttpRequestParser parser;
        httpparser::HttpRequestParser::ParseResult res = parser.parse(req->connection->request, (char*) req->buf, ((char*) req->buf) + req->res);
        if (res == httpparser::HttpRequestParser::ParsingCompleted) {
            printf("HTTP message parsed!\n");
            add_write_to_request(req->connection, req->buf, req->res);
        } else if (res == httpparser::HttpRequestParser::ParsingIncompleted) {
            printf("ERROR: Unimplemented (message with more than 8128 bytes\n");
            exit(1);
        } else {
            printf("ERROR Parsing HTTP request, closing connection");
            add_close_from_request(req->connection);
            add_close_to_request(req->connection);
        }
    } else {
        req->connection->closing = true;
        free(req->buf);
        add_close_from_request(req->connection);
        add_close_to_request(req->connection);
    }
    free(req);
}

void handle_write_to_request(struct request* req) {
    if (options.verbose) printf("handle write to request\n");
    if (req->connection->closing) {
        free(req->buf);
        free(req);
        return;
    }
    int remn = req->size - req->res;
    if (remn == 0) {
        free(req->buf);
        add_read_to_request(req->connection);
    } else if (remn < req->size) {   
        memcpy(req->buf, req->buf + req->size - remn, remn); // remainder
        add_write_to_request(req->connection, req->buf, remn);
    } else {
        req->connection->closing = true;
        free(req->buf);
        add_close_from_request(req->connection);
        add_close_to_request(req->connection);
    }
    free(req);
}

void handle_read_to_request(struct request* req) {
    if (options.verbose) printf("handle read to request\n");
    if (req->connection->closing) {
        free(req->buf);
        free(req);
        return;
    }
    if (req->res > 0) {
        httpparser::HttpResponseParser parser;
        httpparser::HttpResponseParser::ParseResult res = parser.parse(req->connection->response, (char*) req->buf, (char*) req->buf + req->res);
        if (res == httpparser::HttpResponseParser::ParsingCompleted) {            
            add_write_to_request(req->connection, req->buf, req->res);
        } else if (res == httpparser::HttpResponseParser::ParsingIncompleted) {
            // read next bytes
            printf("ERROR: Unimplemented (message with more than 8128 bytes");
            exit(1);
        } else {
            printf("ERROR Parsing HTTP response, closing connection");
            add_close_from_request(req->connection);
            add_close_to_request(req->connection);
        } 
        add_write_from_request(req->connection, req->buf, req->res);
    } else {
        req->connection->closing = true;
        free(req->buf);
        add_close_from_request(req->connection);
        add_close_to_request(req->connection);
    }
    free(req);
}

void handle_write_from_request(struct request* req) {
    if (options.verbose) printf("handle write from request\n");
    if (req->connection->closing) {
        free(req->buf);
        free(req);
        return;
    }
    int remn = req->size - req->res;
    if (remn == 0) {
        free(req->buf);
        add_read_to_request(req->connection);
    } else if (remn < req->size) {        
        memcpy(req->buf, req->buf + req->size - remn, remn);
        add_write_to_request(req->connection, req->buf, remn);
    } else {
        req->connection->closing = true;
        free(req->buf);
        add_close_from_request(req->connection);
        add_close_to_request(req->connection);
    }
    free(req);
}

void handle_close_from_request(struct request* req) {
    free(req);
}

void handle_close_to_request(struct request* req) {
    // deallocate connection structure
    free(req->connection);
    free(req);
}

void server_loop() {
    // insert first accept request event to start the chain
    add_accept_request();
    while (1) {
        if (options.verbose) printf("wait for event\n");
        struct io_uring_cqe *cqe;

        int ret = io_uring_wait_cqe(&ring, &cqe);
        if (ret < 0) {
            fatal_error("io_uring_wait_cqe");
        }
        struct request *req = (struct request*) cqe->user_data;
        if (options.verbose) printf("received event: %d\n", req->event_type);
        if (cqe->res < 0) {
            fprintf(stderr, "Async request failed: %s for event: %d\n", strerror(-cqe->res), req->event_type);
            exit(1);
        }
        req->res = cqe->res;

        switch (req->event_type) {
        case EVENT_TYPE_ACCEPT:
            handle_accept_request(req);
            break;
        case EVENT_TYPE_CONNECT:
            handle_connect_request(req);
            break;
        case EVENT_TYPE_CLIENT_READ:
            handle_read_from_request(req);
            break;
        case EVENT_TYPE_SERVER_WRITE:
            handle_write_to_request(req);
            break;
        case EVENT_TYPE_SERVER_READ:
            handle_read_to_request(req);
            break;
        case EVENT_TYPE_CLIENT_WRITE:
            handle_write_from_request(req);
            break;
        case EVENT_TYPE_CLOSE_TO:
            handle_close_to_request(req);
            break;
        case EVENT_TYPE_CLOSE_FROM:
            handle_close_from_request(req);
            break;
        }
        io_uring_cqe_seen(&ring, cqe); // mark request as processed
    }
}

void sigint_handler(int signo) {
    printf("Interrupted. Shutting down.\n");
    io_uring_queue_exit(&ring);
    exit(0);
}

void set_default_options() {
    options.master_host = NULL;
    options.master_port = 0;
    options.replica_host = NULL;
    options.replica_port = 0;
    options.listen_port  = 0;
    options.verbose      = false;
}

void parse_options(int argc, char* argv[]) {
    static struct option long_options[] = {
    /*   NAME       ARGUMENT           FLAG  SHORTNAME */
        {"listen-port",    required_argument, NULL, 'l'},
        {"master-host",    required_argument, NULL, 'f'},
        {"master-port",    required_argument, NULL, 'p'},
        {"replica-host",   required_argument, NULL, 'g'},
        {"replica-port",   required_argument, NULL, 'q'},
        {"verbose",        no_argument,       NULL, 'v'},
        {"help",           no_argument,       NULL, 'h'},
        {NULL,             0,                 NULL,  0}
    };
    int option_index = 0;
    int c;
    bool hasError = false;

    while ((c = getopt_long(argc, argv, "l:f:p:g:q:hv", long_options, &option_index)) != -1) {
        int this_option_optind = optind ? optind : 1;
        switch (c) {
        case 'l':
            options.listen_port = atoi(optarg);
            if (options.listen_port < 1 || options.listen_port > 65535) {
                printf("listen-port must specify a port in range [1..65535]\n");
                hasError = true;
            }
            break;
        case 'f':
            options.master_host = optarg;
            if (strlen(options.master_host) == 0) {
                printf("master-host host must at least container contain one character\n");
                hasError = true;
            }
            break;
        case 'p':
            options.master_port = atoi(optarg);
            if (options.master_port <= 1 || options.master_port > 65535) {
                printf("master-port port must specify a port in range [1..65535]\n");
                hasError = true;
            }
            break;
        case 'g':
            options.replica_host = optarg;
            if (strlen(options.replica_host) == 0) {
                printf("replica-host host must at least container contain one character\n");
                hasError = true;
            }
            break;
        case 'q':
            options.replica_port = atoi(optarg);
            if (options.replica_port <= 1 || options.replica_port > 65535) {
                printf("replica-port must specify a port in range [1..65535]\n");
                hasError = true;
            }
            break;
        case 'v':
            options.verbose = true;
            break;
        case 'h':
            printf("%s", usage_message);
            break;
        }
    }

    if (optind < argc) {
        printf ("unexpected arguments: ");
        while (optind < argc) {
            printf ("%s ", argv[optind++]);
        }
        printf ("\n");
        hasError = true;
    }

    if (hasError || options.listen_port == 0 || options.master_host == NULL || options.master_port == 0 || options.master_host == NULL || options.replica_port == 0) {
        printf(usage_message, argv[0]);
        exit(1);
    }
}

void resolve_forward_address(struct addrinfo** addrinfo_ptr, char* forward_host, int forward_port) {
    struct addrinfo hints;
    memset(&hints, 0, sizeof(hints));
    hints.ai_family = AF_UNSPEC;
    hints.ai_socktype = SOCK_STREAM;
    hints.ai_flags = 0;
    hints.ai_protocol = 0;
    hints.ai_canonname = NULL;
    hints.ai_addr = NULL;
    hints.ai_next = NULL;

    char port[20];
    snprintf(port, 19, "%d", forward_port);
    port[19] = 0;

    if (getaddrinfo(forward_host, port, &hints, addrinfo_ptr) != 0) {
        fatal_error("getaddrinfo");
    }

    if (*addrinfo_ptr == NULL) {
        fatal_error("getaddrinfo: no host found");
    }
}


int main(int argc, char* argv[]) {
    set_default_options();
    parse_options(argc, argv);

    printf("statistics monitoring proxy\n");
    if (options.verbose) {
        printf("  listening on:  %d\n", options.listen_port);
        printf("  forwarding to master: %s:%d\n", options.master_host, options.master_port);
        printf("  forwarding to replica: %s:%d\n", options.replica_host, options.replica_port);
        printf("  verbose mode:  %s\n", options.verbose ? "true" : "false");
    }
    
    resolve_forward_address(&globals.master_addrinfo, options.master_host, options.master_port);
    resolve_forward_address(&globals.server_addrinfo2, options.replica_host, options.replica_port);
    setup_listening_socket();
    signal(SIGINT, sigint_handler);
    io_uring_queue_init(QUEUE_DEPTH, &ring, 0);
    server_loop();

    return 0;
}
