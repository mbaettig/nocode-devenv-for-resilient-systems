
const dbo = require('./mongoUtils');
const httpJson = require('./httpJsonUtils');
const { ObjectId } = require("mongodb");
const assert = require('assert');
const user = require('./checkUser');

module.exports = {  
    addCollection : function(router, collection) {
        router.get('/', function (req, res, next) {
            httpJson.performAction(async function (body, params, query) {
                return await getObjects(user.getUserId(query.token), collection, query);
            }, req, res, 200);
        });
        router.get('/:id', function (req, res, next) {
            httpJson.performAction(async function (body, params, query) {
                return await getObject(params.id, user.getUserId(query.token), collection);
            }, req, res, 200);
        });
        router.post('/', function (req, res, next) {
            httpJson.performAction(async function (body, params, query) {
                return await addObject(user.getUserId(query.token), body, collection);
            }, req, res, 201);
        });
        router.delete('/:id', function (req, res, next) {
            httpJson.performAction(async function (body, params, query) {
                await deleteObject(params.id, user.getUserId(query.token), collection);
            }, req, res, 200);
        });
        router.put('/:id', function (req, res, next) {
            httpJson.performAction(async function (body, params, query) {
                return await updateObject(params.id, user.getUserId(query.token), body, collection);
            }, req, res, 200);
        });
    }
}

async function addObject (userId, object, collection) {
    assert(userId !== "", "userId expected");
    assert(collection !== "", "collection expected");
    
    const db = await dbo.getDb();    
    
    try {
        object.userId = userId;
        const result = await db.collection(collection).insertOne(object);
        object._id = result.insertedId;
        return object;
    } catch(error) {
        if (error.code == 11000 /* unique constraint violation */) {
            throw new httpJson.HTTPError(400, 'Objects must have unique names. Another object of type \'' + collection + '\' with name \'' + object.name + '\' already exists');
        }
        throw error;
    }
}

async function getObjects(userId, collection, query) {
    assert(userId !== "", "userId expected");
    assert(collection !== "", "collection expected");

    const db = await dbo.getDb();

    // setup query parameters
    let selector = { "userId" : { $eq : userId } };
 
    // perform query
    const result = await db.collection(collection).find(selector);
    return result.toArray();
}

async function getObject(id, userId, collection) {
    assert(id !== "", "id expected");
    assert(userId !== "", "userId expected");
    assert(collection !== "", "collection expected");
    
    const db = await dbo.getDb();
    return getObjectInternal(db, id, userId, collection);
}
    
async function updateObject(id, userId, object, collection) {
    assert(id !== "", "id expected");
    assert(userId !== "", "userId expected");
    assert(collection !== "", "collection expected");

    // ensure _id and userId properties remain as is
    object._id = createObjectIdFromHexString(id);
    object.userId = userId;
    // perform action
    const db = await dbo.getDb();
    const result = await db.collection(collection).replaceOne({ _id : { $eq : object._id }, userId : { $eq : object.userId } }, object);
    
    // report proper error in case no update was performed
    if (result.matchedCount === 0) {
        object = getObjectInternal(db, id, userId, collection);
        // no error: concurrent delete/insert: return newest version
    }

    return object;
}

async function deleteObject(id, userId, collection) {
    assert(id !== "", "id expected");
    assert(userId !== "", "userId expected");
    assert(collection !== "", "collection expected");

    // perform action
    const db = await dbo.getDb();
    const result = await db.collection(collection).deleteOne({ _id : { $eq : createObjectIdFromHexString(id) }, userId : { $eq : userId } });

    // report proper error in case no update was performed
    if (result.deletedCount === 0) {
        getObjectInternal(db, id, userId, collection);
    }    
}

async function getObjectInternal(db, id, userId, collection) {
    assert(id !== "", "id expected");
    assert(userId !== "", "userId expected");
    assert(collection !== "", "collection expected");
    
    const selector = { _id : { $eq : createObjectIdFromHexString(id) } };
    const result = await db.collection(collection).findOne(selector);

    // check result (order is relevant)
    httpJson.checkOrFail(result != null, 404, "Ressource not found");
    httpJson.checkOrFail(result.userId === userId, 403, "Access to ressource denied")

    return result;
}

function createObjectIdFromHexString(hexString) {
    try {
        return ObjectId.createFromHexString(hexString);
    } catch (err) {
        throw new httpJson.HTTPError(404, "Ressource not found");
    }
}