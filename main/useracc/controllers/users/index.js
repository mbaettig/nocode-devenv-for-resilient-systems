/**
 * Copyright 2022 Lucerne Applied University of Sciences / IoT S&S Research Group
 *
 * Licensed under the Apache License, Version 2.0 (the "License");
 * you may not use this file except in compliance with the License.
 * You may obtain a copy of the License at
 * 
 *     http://www.apache.org/licenses/LICENSE-2.0
 *
 * Unless required by applicable law or agreed to in writing, software
 * distributed under the License is distributed on an "AS IS" BASIS,
 * WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
 * See the License for the specific language governing permissions and
 * limitations under the License.
 * 
 */

const express    = require('express');
const bcrypt     = require('bcryptjs');
const jwt        = require("jsonwebtoken");
const router     = express.Router();
const nodemailer = require('nodemailer');
const hat        = require('hat');

const dbo = require('../../common/mongoUtils');

/**
 * Registers a new user:
 * - generate entry in the database if email is not already registered.
 * - Send verification email.
 * 
 * @param req HTTP request instance.
 * @returns HTTP response body.
 */
async function registerUser(req) {
    const db = await dbo.getDb();

    const salt = await bcrypt.genSalt(10); // TODO: Calibrate for the call to take around 100ms
    const hashedPassword = await bcrypt.hash(req.body.password, salt);
    const token = hat();
    const tokenTimestamp = Date.now();

    try {       
        await db.collection('useraccounts').insertOne({
            email           : req.body.email,
            hashedPassword  : hashedPassword,
            token           : token,
            tokenTimestamp  : tokenTimestamp,
            isVerified      : false
        });
        if (await sendMail(req.body.email, token)) {
            return createSuccessResponse();
        } else {
            return createInternalErrorResponse('error sending verification mail to ' + req.body.email);
        }
    } catch (error) {
        if (error.code == 11000 /* DUPLICATE KEY */) {
            return createUserErrorResponse('user already exists');
        }
        throw error;
    }
}

/**
 * Authenticates the user by hashing the plain text password transmitted by the current reqeust
 * and comparing this hash to the hash stored on the database and checks whether the users is
 * always verified.
 * 
 * The fnction generates a HTTP-Response:
 * In case the user is authenticated and verified, the function returns a json web token.
 * In anycase the response contains the information on whether the user is verifed or not.
 * 
 * @param {*} req HTTP request instance.
 * @returns HTTP response body.
 */
async function authenticateUser(req) {
    const db = await dbo.getDb();

    const useraccount = await db.collection('useraccounts').findOne({ email: { $eq : req.body.email }});    
    // no row found (client error: user account does not exist)
    if (useraccount == null) return createUnauthorizedErrorResponse('incorrect email or password');

    const matches = await bcrypt.compare(req.body.password, useraccount.hashedPassword);    
    if (matches) {
        if (useraccount.isVerified) {
            let token = jwt.sign({ userId: useraccount._id }, "TODO: extract to config", {
                expiresIn: 86400 // 24 hours (TODO: reduce and add automatic renwal)
            });
            return createSuccessResponse({ verifed: true, jwt: token });
        } else {
            return createSuccessResponse({ verifed: false, jwt: null });
        }
    } else {
        return createUnauthorizedErrorResponse('incorrect email or password');
    }
}

/**
 * Sends an mail to the recipient containing the link to verify the account.
 * 
 * @param {*} email must be a valid email address (not null).
 * @param {*} token must be equals the user token (not null).
 * @returns true if mail is successfuly sent otherwise false.
 */
async function sendMail(email, token) {
    const transporter = nodemailer.createTransport({
        port: 1025,
        host: 'localhost',
        tls: {
            rejectUnauthorized: false
        },
    });
    const message = {
        from:    'noreply@domain.com',
        to:      email,
        subject: 'Confirm Email',
        text:    'Please confirm your email',
        html:    '<p>Please confirm your email: http://localhost:8080/validate?email=' + email + '&token=' + token + '</p>'
    };
    try {
        const result = await transporter.sendMail(message);
        console.log('Message sent: %s', result.messageId);
        return true;
    } catch (error) {
        // TODO: categorize into user and server errors
        console.log('Error sending message to : %s', error);
        return false;
    }
}

async function verifyUser(req) {
    const db = await dbo.getDb();
    
    const useraccount = db.collection('useraccounts').findOne({ email: { $eq : req.body.email }});
    
    // no row found (client error: user account does not exist)
    if (useraccount == null) return createUnauthorizedErrorResponse('incorrect email or password');
    if (useraccount.token != req.body.token) return createUserErrorResponse('token does not match');

    useraccount.isVerified = true;
    const result = await db.getDb().collection('useraccounts').replaceOne({ email: { $eq : req.body.email }}, useraccount);
    // TODO: Assert one doucment is updated, otherwise create internal error message
    console.log(result);

    return createSuccessResponse();
}

function performAction(action, req, res) {
    action(req)
    .then(response => sendJson(res, response), response => sendJson(res, response))
    .catch(err => {
        console.log(err);
        sendJson(res, createInternalErrorResponse());
    });
}

function createSuccessResponse() {
    return createResponseWithBody(200, {});
}

function createSuccessResponse(body) {
    return createResponseWithBody(200, body);
}

function createUserErrorResponse(message) {
    return createResponseWithMessage(400, message);
}

function createUnauthorizedErrorResponse(message) {
    return createResponseWithMessage(401, message);
}

function createInternalErrorResponse(message) {
    console.log(message);
    return createResponseWithMessage(500, 'internal server error');
}

function createResponseWithMessage(httpCode, message) {
    return createResponseWithBody(httpCode, { message: message })
}

function createResponseWithBody(httpCode, body) {
    return { httpCode: httpCode, body: body };
}

function sendJson(res, response) {
    if (!('httpCode' in response)) {
        response = createInternalErrorResponse(response);
    }
    res.status(response.httpCode);
    res.json(response.body);
}

router.post('/register', function (req, res, next) {
    performAction(registerUser, req, res);
});

router.post('/validate', function (req, res, next) {
    performAction(verifyUser, req, res);
});

router.post('/login', function (req, res, next) {
    performAction(authenticateUser, req, res);
});

module.exports = router;
