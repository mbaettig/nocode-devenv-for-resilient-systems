/**
 * Copyright 2022 Lucerne Applied University of Sciences / IoT S&S Research Group
 *
 * Licensed under the Apache License, Version 2.0 (the "License");
 * you may not use this file except in compliance with the License.
 * You may obtain a copy of the License at
 * 
 *     http://www.apache.org/licenses/LICENSE-2.0
 *
 * Unless required by applicable law or agreed to in writing, software
 * distributed under the License is distributed on an "AS IS" BASIS,
 * WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
 * See the License for the specific language governing permissions and
 * limitations under the License.
 * 
 */

const validUrl = require('valid-url');
const loadProject = require('../common/loadProject');
const elementNamePattern = /^[a-zA-Z0-9][a-zA-Z0-9_.-]*$/;

const projectValidatorFactory = {
    create() {
        return {
            project: null,
            errors: [],
            addError(elementInfo, property, message) {
                this.errors.push({
                    elementInfo: elementInfo,
                    property: property,
                    message: message
                });
            },
            async validate(task) {
                const project = await loadProject(task.name, task.token);
                this.project = project;
                this.validateProject(project);
                return this.errors;
            },
        
            validateProject(project) {
                const names = new Set();
                const duplicates = new Set();
                for (const element of project.elements) {
                    const fullName = element.name + '/' + element.type;
                    if (names.has(fullName)) {
                        duplicates.add(fullName);
                    }
                    names.add(fullName);
                }
                for (const element of project.elements) {
                    const fullName = element.name + '/' + element.type;
                    const elementInfo = this.createElementInfo(element.name, element.type);
                    if (duplicates.has(fullName)) {
                        this.addError(elementInfo, 'name', 'duplicate names are not allowed');
                    }
                    if (!elementNamePattern.test(element.name)) {
                        this.addError(elementInfo, 'name', 'invalid element name');
                    }
        
                    switch (element.type) {
                        case 'environment': this.validateEnvironment(element); break;
                        case 'system':      this.validateSystem(element); break;
                        case 'component':   this.validateComponent(element); break;
                        case 'interface':   this.validateInterface(element); break;
                        case 'service':     this.validateService(element); break;
                        case 'resource':    this.validateResource(element); break;
                    }
                }
            },
        
            validateEnvironment(environment) {  
                const elementInfo = this.createElementInfo(environment.name, environment.type, null );                
                if (environment.resource == '') {
                    this.addError(elementInfo, 'resource', 'Resource must be specified');
                }

                const duplicateInstanceNames = this.getDuplicateInstanceNames(environment);   
                for (const instance of environment.instances) {
                    this.validateInstance(elementInfo, instance, duplicateInstanceNames);
                }
                for (const connection of environment.connections) {
                    this.validateConnection(elementInfo, connection, environment);
                }
            },
        
            validateSystem(system) {
                const elementInfo = this.createElementInfo(system.name, system.type, null );  
                const duplicateInstanceNames = this.getDuplicateInstanceNames(system);
                for(const instance of system.instances) {
                    this.validateInstance(elementInfo, instance, duplicateInstanceNames);
                }
                for (const connection of system.connections) {
                    this.validateConnection(elementInfo, connection, system);
                }
            },

            getDuplicateInstanceNames(element) {
                const duplicateInstanceNames = new Set();
                const instanceNames = new Set();
                for(const instance of element.instances) {
                    if (!instanceNames.has(instance.name)) {
                        instanceNames.add(instance.name);
                    } else {
                        duplicateInstanceNames.add(instance.name);
                    }
                }
                return duplicateInstanceNames;
            },
        
            validateInstance(parentElementInfo, instance, duplicateInstanceNames) {
                this.createElementInfo(instance.name, instance.type, parentElementInfo);
                this.checkMandatory(parentElementInfo, 'name', instance.name, 'Instance name must be specified');
                if (duplicateInstanceNames.has(instance.name)) {
                    this.addError(parentElementInfo, 'name', 'Instance names must be unique');
                }
                this.checkMandatory(parentElementInfo, 'definition', instance.definition, 'Definition must be specified');
                this.checkMandatory(parentElementInfo, 'definitionType', instance.definitionType, 'Definition type name must be specified (component, system or service)');
                if (instance.definitionType && instance.definitionType !== '' && !['component', 'system', 'service'].includes(instance.definitionType)) {
                    this.addError(parentElementInfo, 'definitionType', 'Definition type must be one of: component, system, or service');
                }
                this.checkResource(parentElementInfo, 'resource', instance.resource);
            },

            validateConnection(parentElementInfo, connection, parent) {
                let fromInterface = null;
                let toInterface = null;
                let isPublicInterface = false;
                let instanceFrom = this.getChildElement(parent, 'instance', connection.instanceFrom);
                if (instanceFrom == null && parent.type == 'environment') {
                    instanceFrom = this.getChildElement(parent, 'publicInterface', connection.connectorFrom)
                    isPublicInterface = instanceFrom != null;
                }
                if (instanceFrom == null && connection.instanceFrom != parent.name) {
                    this.addError(parentElementInfo, 'name', 'cannot locate start instance of connection' + connection.instanceFrom);
                } else if (isPublicInterface) {
                    fromInterface = instanceFrom.interface;
                } else {
                    const definition = instanceFrom == null ? parent : this.getElement(instanceFrom.definition, instanceFrom.definitionType);
                    let connectorFrom = this.getChildElement(definition, 'import', connection.connectorFrom);
                    if (connectorFrom == null) {
                        connectorFrom = this.getChildElement(definition, 'export', connection.connectorFrom);
                    }
                    if (connectorFrom == null) {
                        this.addError(parentElementInfo, 'name', 'cannot locate start connector of connection ' + connection.connectorTo);
                    } else {
                        fromInterface = connectorFrom.interface;
                    }
                }
                
                const instanceTo = this.getChildElement(parent, 'instance', connection.instanceTo);
                if (instanceTo == null && connection.instanceTo != parent.name) {
                    this.addError(parentElementInfo, 'name', 'cannot locate target instance of connection' + connection.instanceTo);
                } else {
                    const definition = instanceTo == null ? parent : this.getElement(instanceTo.definition, instanceTo.definitionType);
                    let connectorTo = this.getChildElement(definition, 'export', connection.connectorTo);
                    if (connectorTo == null) {
                        connectorTo = this.getChildElement(definition, 'import', connection.connectorTo);
                    }
                    if (connectorTo == null) {
                        this.addError(parentElementInfo, 'name', 'cannot locate end connector of connection' + connection.connectorTo);
                    } else {
                        toInterface = connectorTo.interface;
                    }
                }
                if (fromInterface && toInterface && fromInterface != toInterface) {
                    this.addError(parentElementInfo, 'name', 'interfaces of connection do not match');
                }
            },

            validateComponent(component) {
                const elementInfo = this.createElementInfo(component.name, component.type, null );
                this.checkMandatory(elementInfo, 'image', component.image, 'Container image must be specified');
                this.checkMandatory(elementInfo, 'type', component.type, 'Container type must be specified');
                for (imprt of component.imports) {
                    this.validateImport(elementInfo, imprt)
                }
                for (exprt of component.exports) {
                    this.validateExport(elementInfo, exprt);
                }
            },
        
            validateImport(parentElementInfo, imprt) {
                this.createElementInfo(imprt.name, 'import', parentElementInfo);
                this.checkMandatory(parentElementInfo, 'name', imprt.name, 'Name of import must be specified');
                this.checkMandatory(parentElementInfo, 'interface', imprt.interface, 'An interface must be specified');
                if (imprt.interface && imprt.interface !== '') {
                    try {
                        this.getElement(imprt.interface, 'interface');
                    } catch (error) {
                        this.addError(parentElementInfo, 'interface', error.message);
                    }
                }
                this.checkMandatory(parentElementInfo, 'environmentHost', imprt.environmentHost, 'An environment variable to contain the hostname must be specified');
                this.checkMandatory(parentElementInfo, 'environmentPort', imprt.environmentPort, 'An environment variable to contain the port must be specified');
            },

            validateExport(parentElementInfo, exprt) {
                this.createElementInfo(exprt.name, 'export', parentElementInfo);
                this.checkMandatory(parentElementInfo, 'name', exprt.name, 'Name of export must be specified');
                this.checkMandatory(parentElementInfo, 'interface', exprt.interface, 'An interface must be specified');
                if (!exprt.environmentPort || exprt.environmentPort == '') {
                    console.log(exprt);
                }
                this.checkMandatory(parentElementInfo, 'environmentPort', exprt.environmentPort, 'An environment variable to contain the port must be specified');
            },

            validateInterface(interface) {
                // nothing yet
            },

            validateService(service) {
                const elementInfo = this.createElementInfo(service.name, service.type, null );                
                this.checkMandatory(elementInfo, 'url', service.url, 'An URL must be specified');
                if (service.url && service.url != '' && !validUrl.isUri(service.url)) {
                    this.addError(elementInfo, 'url', 'URL is malformed');
                }
            },
        
            validateResource(resource) {
                const elementInfo = this.createElementInfo(resource.name, resource.type, null );
                this.checkMandatory(elementInfo, 'url', resource.url, 'An URL must be specified');
                if (resource.url && resource.url != '' && !validUrl.isUri(resource.url)) {
                    this.addError(elementInfo, 'url', 'URL is malformed');
                }
                if (resource.runtimeType !== 'docker_tls') {
                    this.addError(elementInfo, 'runtimeType', 'Currently only docker_tls is supported as runtime type');
                }
            },

            checkMandatory(elementInfo, field, value, message) {
                if (!value || value === '') {
                    this.addError(elementInfo, field, message);
                }
            },

            checkResource(elementInfo, field, value) {
                if (!value) return;
                const resourceAssignments = value.split(',');
                for (const resourceAssignment of resourceAssignments) {
                    const parts = resourceAssignment.split(';');
                    try {
                        this.getElement(parts[0], 'resource');
                    } catch (error) {
                        this.addError(elementInfo, 'resource', error.message);
                    }
                    if (parts.length == 1) {
                        this.addError(elementInfo, field, 'resource assignment ' + parts[0] + ' is missing the scaling factor');
                    } else if (parts[1] - 0 == NaN || parts[1] - 0 != parseInt(parts[1])) {
                        this.addError(elementInfo, field, 'the scaling factor of resource assignment ' + parts[0] + ' is not an integer');
                    }
                }
            },

            getElement(name, type) {
                const foundElements = this.project.elements.filter((element) => {
                    return element.name === name && element.type == type;
                });
                if (foundElements.length == 0) {
                    throw { message: `no '${type}' with name '${name}' found` };
                } else if (foundElements.length > 1) {
                    throw { message: `more than one '${type}' with name '${name}'` };
                } else {
                    return foundElements[0];
                }
            },

            getChildElement(parent, type, name) {
                const foundElements = parent[type + 's'].filter((element) => {
                    return element.name === name;
                });
                return foundElements.length == 1 ? foundElements[0] : null;
            },

            createElementInfo(name, type, parent = null) {
                const elementInfo = {
                    name: name,
                    type: type,
                    child: null
                };
                if (parent) {
                    parent.child = elementInfo;
                }
                return elementInfo;
            }
        }
    }
}

module.exports = projectValidatorFactory;
