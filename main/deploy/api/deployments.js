/**
 * Copyright 2022 Lucerne Applied University of Sciences / IoT S&S Research Group
 *
 * Licensed under the Apache License, Version 2.0 (the "License");
 * you may not use this file except in compliance with the License.
 * You may obtain a copy of the License at
 * 
 *     http://www.apache.org/licenses/LICENSE-2.0
 *
 * Unless required by applicable law or agreed to in writing, software
 * distributed under the License is distributed on an "AS IS" BASIS,
 * WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
 * See the License for the specific language governing permissions and
 * limitations under the License.
 * 
 */

const createTaskProcessor = require('../common/taskProcessor');
const projectValidatorFactory = require('../validation/validation');
const projectDeploymentFactory = require('../deployment/deployment');

const deploymentApi = createTaskProcessor('deployment', async (task) => {
    try {
        const projectValidator = projectValidatorFactory.create();
        task.result = await projectValidator.validate(task);
        console.log(task.result);
        if (task.result.length === 0) {
            const projectDeployment = projectDeploymentFactory.create();
            await projectDeployment.deploy(task);
        }
        task.status = 'done';
    } catch (error) {
        console.log(error);
        task.status = 'failed';
    }
});

module.exports = deploymentApi;
