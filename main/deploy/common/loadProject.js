/**
 * Copyright 2022 Lucerne Applied University of Sciences / IoT S&S Research Group
 *
 * Licensed under the Apache License, Version 2.0 (the "License");
 * you may not use this file except in compliance with the License.
 * You may obtain a copy of the License at
 * 
 *     http://www.apache.org/licenses/LICENSE-2.0
 *
 * Unless required by applicable law or agreed to in writing, software
 * distributed under the License is distributed on an "AS IS" BASIS,
 * WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
 * See the License for the specific language governing permissions and
 * limitations under the License.
 * 
 */

const environment = require('./environment');
const ajaxCall = require('./ajaxCall');
const elementDefinitions = require('./elementDefinitions');

const apiBaseUrl = 'http://'+ environment.getStorageHost() + ":" + environment.getStoragePort() + '/';

async function getProjects(token) {
    return await ajaxCall('GET', apiBaseUrl + 'projects', token);
};
async function getElements(projectId, type, token) {
    return await ajaxCall('GET', apiBaseUrl + 'projects/' + projectId  + '/' + type + 's', token);
};
async function getElement(projectId, elementId, type, token) {
    return await ajaxCall('GET', apiBaseUrl + 'projects/' + projectId + '/' + type + 's/' + elementId, token);
};

async function loadProject(projectName, token) {
    const projects = await getProjects(token);
    const projectsFiltered = projects.filter((element) => { return element.name == projectName; });
    if (projectsFiltered.length == 0) {
        throw {
            message: 'Project with name \'' + projectName + '\' does not exist'
        }
    }
    if (projectsFiltered.length != 1) {
        throw {
            message: 'Project with name \'' + projectName + '\' exists multiple times for this user'
        }
    }
    const project = projectsFiltered[0];
    project.elements = [];
    // for each element type: get elements and load each element
    for (const [elementTypeName, elementType] of Object.entries(elementDefinitions)) {
        if (!elementType.isTopLevelElement) continue;
        const elementInfos = await getElements(project.id, elementTypeName, token);
        for(const elementInfo of elementInfos) {
            project.elements.push(await getElement(project.id, elementInfo.id, elementTypeName, token));
        }
    }
    return project;
}

module.exports = loadProject;