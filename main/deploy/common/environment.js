/**
 * Copyright 2022 Lucerne Applied University of Sciences / IoT S&S Research Group
 *
 * Licensed under the Apache License, Version 2.0 (the "License");
 * you may not use this file except in compliance with the License.
 * You may obtain a copy of the License at
 * 
 *     http://www.apache.org/licenses/LICENSE-2.0
 *
 * Unless required by applicable law or agreed to in writing, software
 * distributed under the License is distributed on an "AS IS" BASIS,
 * WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
 * See the License for the specific language governing permissions and
 * limitations under the License.
 * 
 */

const options = {
    listenPort : null,
    storageHost : null,
    storagePort : null,   
    showErrors: false
};

/**
 * Evaluates all environments variables.
 */
function parseEnvironment() {
    if (process.env.STORAGE_HOST) {
        setHost('storageHost', process.env.STORAGE_HOST);
    }
    if (process.env.STORAGE_PORT) {
        setPort('storagePort', process.env.STORAGE_PORT);
    }
    if (process.env.LISTEN_PORT) {
        setPort('listenPort', process.env.LISTEN_PORT);
    }
    if (process.env.SHOW_ERRORS) {
        setBoolean('showErrors', process.env.SHOW_ERRORS);
    }
}

/**
 * Parses all command line options.
 */
function parseCommandline() {
    let index = 2; // skip first two arguments (node interpreter, script)
    while (index < process.argv.length) {        
        switch (process.argv[index++]) {
            case '--listen-port':
            case '-l': {         
                setPort('listenPort', process.argv[index++]);
                break;
            }
            case '--storage-host':
            case '-s': {
                setHost('storageHost', process.argv[index++]);
                break;
            }
            case '--storage-port':
            case '-S': {
                setPort('storagePort', process.argv[index++]);
                break;
            }
            case '--show-errors':
            case '-e': {
                setBoolean('showErrors', process.argv[index++]);
                break;
            }
            case '--help':
            case '-h': {
                printHelp(0);
                break;
            }
        }
    }
}

/**
 * Validate that mandatory options are set.
 */
function validateMandatoryOptions() {
    checkOrFail(options.listenPort != null, 'listenPort', 'listen port must be set');
    checkOrFail(options.storageHost != null, 'storageHost', 'storage host must be set');
    checkOrFail(options.storagePort != null, 'storagePort', 'storage port must be set');
}

/**
 * Parse string containing a hostname or ip store value in options array.
 */
function setHost(optionName, value) {
    checkOrFail(value.length > 0, optionName, value + ' is invalid (must contain at least one character)');
    options[optionName] = value;
}

/**
 * Parse string containing a port into a number and store value in options array.
 */
function setPort(optionName, value) {
    const port = parseInt(value, 10);
    checkOrFail(!isNaN(port) && port >= 0 && port < 65536, optionName, value + ' is not a valid port value (integer in range 0 to 65535)');
    options[optionName] = port;
}

/**
 * Stores true in the options array.
 */
function setBoolean(optionName, value) {
    if (String(value).toLowerCase() === 'true') {
        options[optionName] = true;
    } else if (String(value).toLowerCase() == 'false') {
        options[optionName] = false;
    } else {
        checkOrFail(false, optionName, value + ' is not a valid boolean');
    }
}

/**
 * Check that condition holds true. If cond is false print error message.
 */
function checkOrFail(cond, optionName, text) {
    if (!cond) {
        console.log('ERROR: ' + text + ' (option \'' + optionName + '\')');
        printHelp(1);
    }
}

function printHelp(exitCode) {
    console.log(`
Usage: node server.js [args]

    -l, --listen-port PORT     port on which to listen for incomming connections
    -s, --storage-host HOST    host on which the storage service is running
    -S, --storage-port PORT    port on which the storage service is listening
                               for incomming connections
    -e, --show-errors          show full errors (true / false). enable only in
                               development environments
    -h, --help                 prints this help message

Exit status of 0 means successfull execution, while all other status codes indicate failure.
`);
    process.exit(exitCode);
}

parseEnvironment();
parseCommandline();
validateMandatoryOptions();

const environment = {
    getListenPort: function () {
        return options.listenPort;
    },
    getStorageHost: function () {
        return options.storageHost;
    },
    getStoragePort: function () {
        return options.storagePort;
    },
    showErrors: function () {
        return options.showErrors;
    }
}

module.exports = environment;