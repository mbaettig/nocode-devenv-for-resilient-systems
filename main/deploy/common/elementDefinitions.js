/**
 * Copyright 2022 Lucerne Applied University of Sciences / IoT S&S Research Group
 *
 * Licensed under the Apache License, Version 2.0 (the "License");
 * you may not use this file except in compliance with the License.
 * You may obtain a copy of the License at
 * 
 *     http://www.apache.org/licenses/LICENSE-2.0
 *
 * Unless required by applicable law or agreed to in writing, software
 * distributed under the License is distributed on an "AS IS" BASIS,
 * WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
 * See the License for the specific language governing permissions and
 * limitations under the License.
 * 
 */

const elementDefinitions = {
    component: {
        properties: ['version', 'image'],
        children: ['imports', 'exports'],
        isTopLevelElement: true,
    },
    system: {
        properties: ['version'],
        children: ['instances', 'connections', 'imports', 'exports'],
        isTopLevelElement: true,
    },
    service: {
        properties: ['version', 'url'],
        children: ['imports', 'exports'],
        isTopLevelElement: true,
    },
    interface: {
        properties: ['version'],
        children: [],
        isTopLevelElement: true,
    },
    environment: {
        properties: [ 'location', 'key', 'credentials' ],
        children: [ 'instances', 'connections' ],
        isTopLevelElement: true,
    },
    resource: {
        properties: ['version'],
        children: [],
        isTopLevelElement: true,
    },
    import: {
        properties: ['interface', 'environmentHost', 'environmentPort', 'required'],
        children: [],
        isTopLevelElement: false,
    },
    export: {
        properties: ['interface', 'environmentPort'],
        children: [],
        isTopLevelElement: false,
    },
    instance: {
        properties: ['definition'],
        children: ['imports', 'exports', 'instances'],
        isTopLevelElement: false,
    },
    connection: {
        properties: ['connectorFrom', 'instanceTo', 'connectorTo', 'fromX', 'fromY', 'toX', 'toY'],
        children: [],
        isTopLevelElement: false,
    },
};

module.exports = elementDefinitions;