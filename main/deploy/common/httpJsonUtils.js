/**
 * Copyright 2022 Lucerne Applied University of Sciences / IoT S&S Research Group
 *
 * Licensed under the Apache License, Version 2.0 (the "License");
 * you may not use this file except in compliance with the License.
 * You may obtain a copy of the License at
 * 
 *     http://www.apache.org/licenses/LICENSE-2.0
 *
 * Unless required by applicable law or agreed to in writing, software
 * distributed under the License is distributed on an "AS IS" BASIS,
 * WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
 * See the License for the specific language governing permissions and
 * limitations under the License.
 * 
 */

module.exports = {
    performActionWithNext: async function (action, req, res, next) {
        try {
            await action(req, res, next);
            next();
        } catch(err) {
            if (err instanceof this.HTTPError) {
                sendJson(res, { httpCode: err.httpCode, message: err.message });
            } else {
                sendJson(res, { httpCode: 500, message: err });
            }
        }
    },

    performAction: async function (action, req, res, httpCode = 200) {
        try {
            const response = await action(req, res);
            sendJson(res, response, httpCode);
        } catch(err) {
            if (err instanceof this.HTTPError) {
                sendJson(res, { httpCode: err.httpCode, message: err.message });
            } else {
                console.log(err);
                sendJson(res, { httpCode: 500, message: err });
            }
        }
    },

    checkOrFail: function (condition, httpCode, message) {
        if (!condition) {
            throw new this.HTTPError(httpCode, message);
        }
    },

    HTTPError: class extends Error {
        constructor(httpCode, message) {
            super(message);
            this.httpCode = httpCode;
        }
    }
}

function sendJson(res, response, httpCode) {
    if (response === undefined) {
        res.status(httpCode);
        res.json({});
    } else if (!('httpCode' in response)) {
        res.status(httpCode);
        res.json(response);
    } else {
        res.status(response.httpCode);    
        res.json(response.message);
    }        
}
