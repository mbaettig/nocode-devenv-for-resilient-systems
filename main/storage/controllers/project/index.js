/**
 * Copyright 2022 Lucerne Applied University of Sciences / IoT S&S Research Group
 *
 * Licensed under the Apache License, Version 2.0 (the "License");
 * you may not use this file except in compliance with the License.
 * You may obtain a copy of the License at
 * 
 *     http://www.apache.org/licenses/LICENSE-2.0
 *
 * Unless required by applicable law or agreed to in writing, software
 * distributed under the License is distributed on an "AS IS" BASIS,
 * WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
 * See the License for the specific language governing permissions and
 * limitations under the License.
 * 
 */

const assert = require('assert');
const dbo = require('../../common/mongoUtils');
const express = require('express');
const httpJson = require('../../common/httpJsonUtils');
const user = require('../../common/checkUser');
const { ObjectId } = require("mongodb");

const router = express.Router();
router.all('/(:id)?', function (req, res, next) {
    httpJson.performActionWithNext(async function (req, res) {
        if (req.headers.authorization.startsWith('Bearer ')) {
            res.locals.parentId = user.getUserId(req.headers.authorization.substring(7));
        } else {
            httpJson.checkOrFail(req.query.token != null, 401, "token must be set");
        }         
    }, req, res, next);
});
addCollection(router, 'projects');

router.all('/:projectId/*', function (req, res, next) {
    httpJson.performActionWithNext(async function (req, res) {
        if (req.headers.authorization.startsWith('Bearer ')) {
            await getObject(req.params.projectId, user.getUserId(req.headers.authorization.substring(7)), 'projects');
            res.locals.parentId = req.params.projectId;
        } else {
            httpJson.checkOrFail(req.query.token != null, 401, "token must be set");
        }
    }, req, res, next);
});
router.use('/:projectId/components', addCollection(express.Router(), 'components'));
router.use('/:projectId/environments', addCollection(express.Router(), 'environments'));
router.use('/:projectId/interfaces', addCollection(express.Router(), 'interfaces'));
router.use('/:projectId/resources', addCollection(express.Router(), 'resources'));
router.use('/:projectId/services', addCollection(express.Router(), 'services'));
router.use('/:projectId/systems', addCollection(express.Router(), 'systems'));

module.exports = router;

function addCollection(router, collection) {
    router.get('/', function (req, res, next) {
        httpJson.performAction(async function (req, res) {
            return await getObjects(res.locals.parentId, collection, req.query);
        }, req, res, 200);
    });
    router.get('/:id', function (req, res, next) {
        httpJson.performAction(async function (req, res) {
            return await getObject(req.params.id, res.locals.parentId, collection);
        }, req, res, 200);
    });
    router.post('/', function (req, res, next) {
        httpJson.performAction(async function (req, res) {
            return await addObject(res.locals.parentId, req.body, collection);
        }, req, res, 201);
    });
    router.delete('/:id', function (req, res, next) {
        httpJson.performAction(async function (req, res) {
            await deleteObject(req.params.id, res.locals.parentId, collection);
        }, req, res, 200);
    });
    router.put('/:id', function (req, res, next) {        
        httpJson.performAction(async function (req, res) {
            return await updateObject(req.params.id, res.locals.parentId, req.body, collection);
        }, req, res, 200);
    });
    return router;
};

async function addObject(parentId, object, collection) {
    assert(parentId !== "", "parentId expected");
    assert(collection !== "", "collection expected");
    
    const db = await dbo.getDb();
    try {
        object[parentName(collection)] = parentId;
        const result = await db.collection(collection).insertOne(object);
        return toExternalId(object);
    } catch(error) {
        if (error.code == 11000 /* unique constraint violation */) {
            throw new httpJson.HTTPError(400, 'Objects must have unique name within a project. An object of type \'' + collection + '\' with name \'' + object.name + '\' already exists.');
        }
        throw error;
    }
}

async function getObjects(parentId, collection, query) {
    assert(parentId !== "", "parentId expected");
    assert(collection !== "", "collection expected");

    const db = await dbo.getDb();

    // setup query parameters
    const selector = {};
    selector[parentName(collection)] = { $eq : parentId };
 
    // perform query
    const results = await db.collection(collection).find(selector).project({name: 1}).toArray();
    results.forEach(element => toExternalId(element));
    return results;
}

async function getObject(id, parentId, collection) {
    assert(id !== "", "id expected");
    assert(parentId !== "", "parentId expected");
    assert(collection !== "", "collection expected");
    
    const db = await dbo.getDb();
    return toExternalId(await getObjectInternal(db, id, parentId, collection));
}
    
async function updateObject(id, parentId, object, collection) {
    assert(id !== "", "id expected");
    assert(parentId !== "", "parentId expected");
    assert(collection !== "", "collection expected");

    // ensure _id and projectId properties remain as is
    object._id = createObjectIdFromHexString(id);
    object[parentName(collection)] = parentId;
    // perform action
    const db = await dbo.getDb();
    try {
        const result = await db.collection(collection).replaceOne(makeSelector(id, parentId, collection), object);
    
        // report proper error in case no update was performed
        if (result.matchedCount === 0) {
            object = getObjectInternal(db, id, parentId, collection);
            // no error: concurrent delete/insert: return newest version
        }
    } catch(error) {
        if (error.code == 11000 /* unique constraint violation */) {
            throw new httpJson.HTTPError(400, 'Objects must have unique name within a project. An object of type \'' + collection + '\' with name \'' + object.name + '\' already exists.');
        }
        throw error;
    }
    return toExternalId(object);
}

async function deleteObject(id, parentId, collection) {
    assert(id !== "", "id expected");
    assert(parentId !== "", "parentId expected");
    assert(collection !== "", "collection expected");

    // perform action
    const db = await dbo.getDb();
    const result = await db.collection(collection).deleteOne(makeSelector(id, parentId, collection));

    // report proper error in case no update was performed
    if (result.deletedCount === 0) {
        await getObjectInternal(db, id, parentId, collection);
    }    
}

async function getObjectInternal(db, id, parentId, collection) {
    assert(id !== "", "id expected");
    assert(parentId !== "", "parentId expected");
    assert(collection !== "", "collection expected");
    
    const selector = { _id : { $eq : createObjectIdFromHexString(id) } };
    if (collection !== "projects") {
        selector['projectId'] = { $eq : parentId }
    } 
    const result = await db.collection(collection).findOne(selector);

    // check result (order is relevant)    
    httpJson.checkOrFail(result != null, 404, "Resource not found");
    httpJson.checkOrFail(collection !== 'projects' || result.userId === parentId, 403, "Access to resource denied (should never happen");

    return result;
}

function toExternalId(element) {
    element.id = element._id;
    delete element._id;
    return element;
}

function toInternalId(element) {
    element._id = element.id;
    delete element.id;
    return element;
}

function createObjectIdFromHexString(hexString) {
    try {
        return ObjectId.createFromHexString(hexString);
    } catch (err) {
        throw new httpJson.HTTPError(404, "Resource not found");
    }
}

function parentName(collection) {
    return collection === 'projects' ? 'userId' : 'projectId';
}

function makeSelector(id, parentId, collection) {
    const selector = { _id : { $eq : createObjectIdFromHexString(id) }};
    selector[parentName(collection)] = { $eq : parentId };
    return selector;
}