#!/bin/sh
#
# Copyright 2022 Lucerne Applied University of Sciences / IoT S&S Research Group
#
# Licensed under the Apache License, Version 2.0 (the "License");
# you may not use this file except in compliance with the License.
# You may obtain a copy of the License at
# 
#     http://www.apache.org/licenses/LICENSE-2.0
#
# Unless required by applicable law or agreed to in writing, software
# distributed under the License is distributed on an "AS IS" BASIS,
# WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
# See the License for the specific language governing permissions and
# limitations under the License.
# 
#
set -e

#CHECK THAT variable X is set
if [ -z ${HAPROXY_FRONTEND_PORT} ]; then
    echo "Variable HAPROXY_FRONTEND_PORT not set";
    exit 1
fi

if [ -z ${HAPROXY_BACKENDS+x} ]; then
    echo "Variable HAPROXY_BACKENDS not set";
    exit 1
fi

cat << EOF > /tmp/haproxy.conf
global

defaults
    mode tcp
    balance roundrobin
    timeout connect 5000ms
    timeout client 50000ms
    timeout server 50000ms

frontend http-in
    bind *:${HAPROXY_FRONTEND_PORT}
    default_backend servers

backend servers
EOF

NR=1
for ADDRESS in $(echo $HAPROXY_BACKENDS | sed "s/,/ /g")
do
    echo "    server server${NR} ${ADDRESS}" >> /tmp/haproxy.conf
    NR=$((NR + 1))
done

echo "Using configuartion: "

cat /tmp/haproxy.conf

echo ""


exec haproxy -W -db -f /tmp/haproxy.conf