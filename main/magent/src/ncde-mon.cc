/**
 * Copyright 2022 Lucerne Applied University of Sciences / IoT S&S Research Group
 *
 * Licensed under the Apache License, Version 2.0 (the "License");
 * you may not use this file except in compliance with the License.
 * You may obtain a copy of the License at
 * 
 *     http://www.apache.org/licenses/LICENSE-2.0
 *
 * Unless required by applicable law or agreed to in writing, software
 * distributed under the License is distributed on an "AS IS" BASIS,
 * WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
 * See the License for the specific language governing permissions and
 * limitations under the License.
 * 
 */

#include <stdio.h>
#include <netinet/in.h>
#include <string.h>
#include <ctype.h>
#include <unistd.h>
#include <stdlib.h>
#include <signal.h>
#include <netdb.h>
#include <liburing.h>
#include <sys/stat.h>
#include <fcntl.h>
#include <getopt.h>
#include <string.h>
#include <sys/time.h>
#include <string>
#include "urlparser.h"
//#include <uriparser/Uri.h>


#define QUEUE_DEPTH             256
#define READ_SZ                 8192

 // client connected
#define EVENT_TYPE_ACCEPT       0
// server accepeted connection -> issue read from client
#define EVENT_TYPE_CONNECT      1
 // client read -> issue write to server 
#define EVENT_TYPE_CLIENT_READ  2
 // done
#define EVENT_TYPE_CLIENT_WRITE 3
 // read from server -> issue write to client
#define EVENT_TYPE_SERVER_READ  4
 // server write -> issue write to client
#define EVENT_TYPE_SERVER_WRITE 5
 // server write -> issue write to client
#define EVENT_TYPE_CLOSE_FROM   6
 // server write -> issue write to client
#define EVENT_TYPE_CLOSE_TO     7
// handle http reporting
#define EVENT_TYPE_HTTP_CONNECT 8
#define EVENT_TYPE_HTTP_READ    9
#define EVENT_TYPE_HTTP_WRITE  10
#define EVENT_TYPE_HTTP_CLOSE  11

struct options {
    int listen_port;
    char* forward_host;
    int forward_port;
    char* node_name;
    char* report_url;
    bool verbose;
} options;

struct globals {
    int server_socket;
    struct addrinfo *reporting_address;
    char reporting_host[1024];
    char reporting_port[10];
    long bytes_read;
    long bytes_written;
    long requests;
} globals;

struct connection {
    int from_socket;
    int to_socket;
    bool closing;
    int direction;
};

struct request {
    struct connection* connection;
    int event_type;
    unsigned char* buf;
    int res;
    int size;
};

struct io_uring ring;
struct addrinfo *result;

const char *usage_message = \
    "usage: %s <args>\r\n"
    "--node-name=<hostname>\r\n"
    "--listen-port=<port>\r\n"
    "--forward-host=<hostname>\r\n"
    "--forward-port=<port>\r\n"
    "--report-url=url\r\n"
    "\r\n";

// prints the system call and the error details exits with exit code 1 (error).
void fatal_error(const char *syscall) {
    perror(syscall);
    exit(1);
}

// helper function for cleaner looking code.
void *zh_malloc(size_t size) {
    void *buf = malloc(size);
    if (!buf) {
        fatal_error("Fatal error: unable to allocate memory.\n");
    }
    return buf;
}

// sets up the main listening socket used by the web server
void setup_listening_socket() {

    int sock = socket(AF_INET, SOCK_STREAM, 0);
    if (sock == -1) {
        fatal_error("socket()");
    }
    int enable = 1;
    if (setsockopt(sock, SOL_SOCKET, SO_REUSEADDR, &enable, sizeof(int)) < 0) {
        fatal_error("setsockopt(SO_REUSEADDR)");
    }

    struct sockaddr_in srv_addr;
    memset(&srv_addr, 0, sizeof(srv_addr));
    srv_addr.sin_family = AF_INET;
    srv_addr.sin_port = htons(options.listen_port);
    srv_addr.sin_addr.s_addr = htonl(INADDR_ANY);

    // bind to a port and turn this socket into a listening socket
    if (bind(sock, (const struct sockaddr*) &srv_addr, sizeof(srv_addr)) < 0) {
        fatal_error("bind()");
    }

    if (listen(sock, 10) < 0) {
        fatal_error("listen()");
    }
    globals.server_socket = sock;
}


struct request* make_request(struct connection* connection, int event_type, unsigned char* buf, int size) {
    struct request *req = (struct request*) malloc(sizeof(*req));
    req->connection = connection;
    req->event_type = event_type;
    req->buf = buf;
    req->size = size;
    return req;
}

int create_client_socket(struct addrinfo* ai) {
    int clientfd = socket(ai->ai_family, ai->ai_socktype, ai->ai_protocol);
    if (clientfd == -1) {
        fatal_error("create_client_socket: socket(AF_INET, SOCK_STREAM, 0)");
    }
    return clientfd;
}

struct connection* create_connection(int from_socket, struct addrinfo* ai) {
    struct connection* connection = (struct connection*) (malloc(sizeof (struct connection)));
    connection->from_socket = from_socket;
    connection->to_socket = create_client_socket(ai);
    connection->closing = false;
    connection->direction = 1;
    return connection;
}

void add_accept_request() {
    if (options.verbose) printf("enqueue accept request\n");
    struct request *req = make_request(NULL, EVENT_TYPE_ACCEPT, (unsigned char*)malloc(READ_SZ), 0);
    struct io_uring_sqe *sqe = io_uring_get_sqe(&ring);
    io_uring_prep_accept(sqe, globals.server_socket, NULL, NULL, 0);
    io_uring_sqe_set_data(sqe, req);
    io_uring_submit(&ring);
}

void add_connect_request(struct connection* connection, struct sockaddr *client_addr, socklen_t client_addr_len) {
    if (options.verbose) printf("enqueue connection request\n");
    struct request *req = make_request(connection, EVENT_TYPE_CONNECT, NULL, 0);
    struct io_uring_sqe *sqe = io_uring_get_sqe(&ring);
    io_uring_prep_connect(sqe, connection->to_socket, client_addr, client_addr_len);    
    io_uring_sqe_set_data(sqe, req);
    io_uring_submit(&ring);
}

void add_read_from_request(struct connection* connection) {
    if (options.verbose) printf("enqueue read from request\n");
    struct request *req = make_request(connection, EVENT_TYPE_CLIENT_READ, (unsigned char*) malloc(READ_SZ), READ_SZ);
    struct io_uring_sqe *sqe = io_uring_get_sqe(&ring);
    io_uring_prep_read(sqe, connection->from_socket, req->buf, req->size, 0);
    io_uring_sqe_set_data(sqe, req);
    io_uring_submit(&ring);
}

void add_write_to_request(struct connection* connection, unsigned char* buf, int size) {
    if (options.verbose) printf("enqueue write to request\n");
    struct request *req = make_request(connection, EVENT_TYPE_SERVER_WRITE, buf, size);
    struct io_uring_sqe *sqe = io_uring_get_sqe(&ring);
    io_uring_prep_write(sqe, connection->to_socket, req->buf, size, 0);
    io_uring_sqe_set_data(sqe, req);
    io_uring_submit(&ring);
}

void add_read_to_request(struct connection* connection) {
    if (options.verbose) printf("enqueue read to request\n");
    struct request *req = make_request(connection, EVENT_TYPE_SERVER_READ, (unsigned char*) malloc(READ_SZ), READ_SZ);
    struct io_uring_sqe *sqe = io_uring_get_sqe(&ring);
    io_uring_prep_read(sqe, connection->to_socket, req->buf, req->size, 0);
    io_uring_sqe_set_data(sqe, req);
    io_uring_submit(&ring);
}

void add_write_from_request(struct connection* connection, unsigned char* buf, int size) {
    if (options.verbose) printf("enqueue write from request\n");
    struct request *req = make_request(connection, EVENT_TYPE_CLIENT_WRITE, buf, size);
    struct io_uring_sqe *sqe = io_uring_get_sqe(&ring);
    io_uring_prep_write(sqe, connection->from_socket, req->buf, size, 0);
    io_uring_sqe_set_data(sqe, req);
    io_uring_submit(&ring);
}

void add_close_from_request(struct connection* connection) {
    if (options.verbose) printf("enqueue close from request\n");
    struct request *req = make_request(connection, EVENT_TYPE_CLOSE_FROM, NULL, 0);
    struct io_uring_sqe *sqe = io_uring_get_sqe(&ring);
    io_uring_prep_close(sqe, connection->from_socket);
    io_uring_sqe_set_data(sqe, req);
    io_uring_submit(&ring);
}

void add_close_to_request(struct connection* connection) {
    if (options.verbose) printf("enqueue close to request\n");
    struct request *req = make_request(connection, EVENT_TYPE_CLOSE_TO, NULL, 0);
    struct io_uring_sqe *sqe = io_uring_get_sqe(&ring);
    io_uring_prep_close(sqe, connection->to_socket);
    io_uring_sqe_set_data(sqe, req);
    io_uring_submit(&ring);
}

void handle_accept_request(struct request* req) {
    if (options.verbose) printf("handle accept request\n");
    struct connection* connection = create_connection(req->res, result);
    add_connect_request(connection, result->ai_addr, result->ai_addrlen);
    add_accept_request();
    free(req);
}

void handle_connect_request(struct request* req) {
    if (options.verbose) printf("handle connect request\n");
    add_read_from_request(req->connection);
    add_read_to_request(req->connection);
    free(req);
}

void handle_read_from_request(struct request* req) {
    if (options.verbose) printf("handle read from request\n");
    if (req->connection->closing) {
        free(req->buf);
        free(req);
        return;
    }
    if (req->res > 0) {
        globals.bytes_read += req->res;
        add_write_to_request(req->connection, req->buf, req->res);
    } else {
        req->connection->closing = true;
        free(req->buf);
        add_close_from_request(req->connection);
        add_close_to_request(req->connection);
    }
    free(req);
}

void handle_write_to_request(struct request* req) {
    if (options.verbose) printf("handle write to request\n");
    if (req->connection->closing) {
        free(req->buf);
        free(req);
        return;
    }
    globals.bytes_written += req->size;
    int remn = req->size - req->res;
    if (remn == 0) {
        if (req->connection->direction == -1) {
            req->connection->direction = 1;
        }
        free(req->buf);
        add_read_from_request(req->connection);
    } else if (remn < req->size) {   
        memcpy(req->buf, req->buf + req->size - remn, remn); // remainder
        add_write_to_request(req->connection, req->buf, remn);
    } else {
        req->connection->closing = true;
        free(req->buf);
        add_close_from_request(req->connection);
        add_close_to_request(req->connection);
    }
    free(req);
}

void handle_read_to_request(struct request* req) {
    if (options.verbose) printf("handle read to request\n");
    if (req->connection->closing) {
        free(req->buf);
        free(req);
        return;
    }
    if (req->res > 0) {
        globals.bytes_read += req->res;
        add_write_from_request(req->connection, req->buf, req->res);
    } else {
        req->connection->closing = true;
        free(req->buf);
        add_close_from_request(req->connection);
        add_close_to_request(req->connection);
    }
    free(req);
}

void handle_write_from_request(struct request* req) {
    if (options.verbose) printf("handle write from request\n");
    if (req->connection->closing) {
        free(req->buf);
        free(req);
        return;
    }
    globals.bytes_written += req->size;
    int remn = req->size - req->res;
    if (remn == 0) {
        if (req->connection->direction == 1) {
            req->connection->direction = -1;
            globals.requests++;
        }
        free(req->buf);
        add_read_to_request(req->connection);
    } else if (remn < req->size) {        
        memcpy(req->buf, req->buf + req->size - remn, remn);
        add_write_to_request(req->connection, req->buf, remn);
    } else {
        req->connection->closing = true;
        free(req->buf);
        add_close_from_request(req->connection);
        add_close_to_request(req->connection);
    }
    free(req);
}

void handle_close_from_request(struct request* req) {
    free(req);
}

void handle_close_to_request(struct request* req) {
    // deallocate connection structure
    free(req->connection);
    free(req);
}

void add_http_connect_request() {
    if (options.verbose) printf("enqueue HTTP connection request\n");
    struct connection* connection = create_connection(0, globals.reporting_address);
    struct request *req = make_request(connection, EVENT_TYPE_HTTP_CONNECT, NULL, 0);
    struct io_uring_sqe *sqe = io_uring_get_sqe(&ring);
    io_uring_prep_connect(sqe, connection->to_socket, globals.reporting_address->ai_addr, globals.reporting_address->ai_addrlen);
    io_uring_sqe_set_data(sqe, req);
    io_uring_submit(&ring);
}

void add_http_write_request(struct connection* connection, unsigned char* buf, int size) {
    if (options.verbose) printf("enqueue HTTP write request\n");        
    struct request *req = make_request(connection, EVENT_TYPE_HTTP_WRITE, buf, size);
    struct io_uring_sqe *sqe = io_uring_get_sqe(&ring);
    io_uring_prep_write(sqe, connection->to_socket, req->buf, size, 0);
    io_uring_sqe_set_data(sqe, req);
    io_uring_submit(&ring);
}

void add_http_read_request(struct connection* connection) {
    if (options.verbose) printf("enqueue HTTP read request\n");
    struct request *req = make_request(connection, EVENT_TYPE_HTTP_READ, (unsigned char*) malloc(READ_SZ), READ_SZ);
    struct io_uring_sqe *sqe = io_uring_get_sqe(&ring);
    io_uring_prep_read(sqe, connection->to_socket, req->buf, req->size, 0);
    io_uring_sqe_set_data(sqe, req);
    io_uring_submit(&ring);
}

void add_http_close_request(struct connection* connection) {
    if (options.verbose) printf("enqueue HTTP close to request\n");
    struct request *req = make_request(connection, EVENT_TYPE_HTTP_CLOSE, NULL, 0);
    struct io_uring_sqe *sqe = io_uring_get_sqe(&ring);
    io_uring_prep_close(sqe, connection->to_socket);
    io_uring_sqe_set_data(sqe, req);
    io_uring_submit(&ring);
}
 

void handle_http_connect_request(struct request* req) {
    if (options.verbose) printf("handle HTTP connect request\n");
    unsigned char* buf = (unsigned char*) malloc(READ_SZ);
    sprintf((char*)buf, "GET /report?node=%s&read=%li&written=%li&request=%li HTTP/1.1\nHost: %s\n\n",
            options.node_name, globals.bytes_read, globals.bytes_written, globals.requests, globals.reporting_host);
    add_http_write_request(req->connection, buf, strlen((char*)buf));
    free(req);
}

void handle_http_write_request(struct request* req) {
    if (options.verbose) printf("handle HTTP write request\n");
    if (req->connection->closing) {
        free(req->buf);
        free(req);
        return;
    }
    int remn = req->size - req->res;
    if (remn == 0) {
        free(req->buf);
        add_http_read_request(req->connection);
    } else if (remn < req->size) {        
        memcpy(req->buf, req->buf + req->size - remn, remn);
        add_http_write_request(req->connection, req->buf, remn);
    } else {
        req->connection->closing = true;
        free(req->buf);
        add_http_close_request(req->connection);
    }
    free(req);
}
void handle_http_read_request(struct request* req) {
    if (options.verbose) printf("handle HTTP read request\n");
    if (req->connection->closing) {
        free(req->buf);
        free(req);
        return;
    }
    add_http_close_request(req->connection);
    free(req);
}

void handle_http_close_request(struct request* req) {
    if (options.verbose) printf("handle HTTP close request\n");
    free(req->connection);
    free(req);
}

time_t get_current_time() {
    struct timeval current_time;
    int result = gettimeofday(&current_time, NULL);
    if (result != 0) {
        fatal_error("gettimeofday");
    }
    return current_time.tv_sec;
}

void server_loop() {
    struct __kernel_timespec timeout;
    timeout.tv_sec = 2;
    timeout.tv_nsec = 0;

    // insert first accept request event to start the chain
    add_accept_request();
    time_t last_time = get_current_time();
    while (1) {
        if (options.verbose) printf("wait for event\n");
        struct io_uring_cqe *cqe;

        int ret = io_uring_wait_cqe_timeout(&ring, &cqe, &timeout);
        if (ret >= 0) {
            struct request *req = (struct request*) cqe->user_data;
            if (options.verbose) printf("received event: %d\n", req->event_type);
            if (cqe->res < 0) {
                fprintf(stderr, "Async request failed: %s for event: %d\n", strerror(-cqe->res), req->event_type);
                exit(1);
            }
            req->res = cqe->res;

            switch (req->event_type) {
            case EVENT_TYPE_ACCEPT:
                handle_accept_request(req);
                break;
            case EVENT_TYPE_CONNECT:
                handle_connect_request(req);
                break;
            case EVENT_TYPE_CLIENT_READ:
                handle_read_from_request(req);
                break;
            case EVENT_TYPE_SERVER_WRITE:
                handle_write_to_request(req);
                break;
            case EVENT_TYPE_SERVER_READ:
                handle_read_to_request(req);
                break;
            case EVENT_TYPE_CLIENT_WRITE:
                handle_write_from_request(req);
                break;
            case EVENT_TYPE_CLOSE_TO:
                handle_close_to_request(req);
                break;
            case EVENT_TYPE_CLOSE_FROM:
                handle_close_from_request(req);
                break;
            case EVENT_TYPE_HTTP_CONNECT:
                handle_http_connect_request(req);
                break;
            case EVENT_TYPE_HTTP_READ:
                handle_http_read_request(req);
                break;
            case EVENT_TYPE_HTTP_WRITE:
                handle_http_write_request(req);
                break;
            case EVENT_TYPE_HTTP_CLOSE:
                handle_http_close_request(req);
                break;
            }
            io_uring_cqe_seen(&ring, cqe); // mark request as processed
        } else if (ret != -ETIME) {
            fatal_error("io_uring_wait_cqe");
        }
        
        time_t current_time = get_current_time();
        if (current_time - last_time > 5) {
            last_time = current_time;
            if (options.verbose) printf("report stats (%li,%li,%li)\n", globals.bytes_read, globals.bytes_written, globals.requests);
            add_http_connect_request();
        }
    }
}

void sigint_handler(int signo) {
    printf("Interrupted. Shutting down.\n");
    io_uring_queue_exit(&ring);
    exit(0);
}

void set_default_options() {
    options.forward_host = NULL;
    options.forward_port = 0;
    options.listen_port  = 0;
    options.report_url   = NULL;
    options.verbose      = false;
}

void parse_options(int argc, char* argv[]) {
    static struct option long_options[] = {
    /*   NAME       ARGUMENT           FLAG  SHORTNAME */
        {"node-name",     required_argument, NULL, 'n'},
        {"listen-port",   required_argument, NULL, 'l'},
        {"forward-host",  required_argument, NULL, 'f'},
        {"forward-port",  required_argument, NULL, 'p'},
        {"report-url",    required_argument, NULL, 'r'},
        {"verbose",       no_argument,       NULL, 'v'},
        {"help",          no_argument,       NULL, 'h'},
        {NULL,            0,                 NULL, 0}
    };
    int option_index = 0;
    int c;
    bool hasError = false;

    while ((c = getopt_long(argc, argv, "l:f:p:r:h", long_options, &option_index)) != -1) {
        int this_option_optind = optind ? optind : 1;
        switch (c) {
        case 'n':
            options.node_name = optarg;
            if (strlen(options.node_name) == 0) {
                printf("node name must at least container contain one character\n");
                hasError = true;
            }
            break;
        case 'l':
            options.listen_port = atoi(optarg);
            if (options.listen_port < 1 || options.listen_port > 65535) {
                printf("listen port must specify a port in range [1..65535]\n");
                hasError = true;
            }
            break;
        case 'f':
            options.forward_host = optarg;
            if (strlen(options.forward_host) == 0) {
                printf("forward host must at least container contain one character\n");
                hasError = true;
            }
            break;
        case 'p':
            options.forward_port = atoi(optarg);
            if (options.forward_port <= 1 || options.forward_port > 65535) {
                printf("listen port must specify a port in range [1..65535]\n");
                hasError = true;
            }
            break;
        case 'r':
            options.report_url = optarg;
            if (strlen(options.report_url) == 0) {
                printf("report url must specify a valid url\n");
                hasError = true;
            }
            break;
        case 'v':
            options.verbose = true;
            break;
        case 'h':
            printf("%s", usage_message);
            break;
        }
    }

    if (optind < argc) {
        printf ("unexpected arguments: ");
        while (optind < argc) {
            printf ("%s ", argv[optind++]);
        }
        printf ("\n");
        hasError = true;
    }

    if (hasError || options.node_name == 0 || options.listen_port == 0 || options.forward_host == NULL || options.forward_port == 0 || options.report_url == NULL) {
        printf(usage_message, argv[0]);
        exit(1);
    }
}

void resolve_forward_address() {
    struct addrinfo hints;
    memset(&hints, 0, sizeof(hints));
    hints.ai_family = AF_UNSPEC;
    hints.ai_socktype = SOCK_STREAM;
    hints.ai_flags = 0;
    hints.ai_protocol = 0;
    hints.ai_canonname = NULL;
    hints.ai_addr = NULL;
    hints.ai_next = NULL;

    char port[20];
    snprintf(port, 19, "%d", options.forward_port);
    port[19] = 0;

    if (getaddrinfo(options.forward_host, port, &hints, &result) != 0) {
        fatal_error("getaddrinfo");
    }

    if (result == NULL) {
        fatal_error("getaddrinfo: no host found");
    }
}

void resolve_reporting_domain() {
    httpparser::UrlParser urlParser;
    std::string url = std::string(options.report_url);
    if (!urlParser.parse(url)) {
        printf("cannot parse reporting url: %s", options.report_url);
        exit(1);
    }
    strncpy(globals.reporting_host, urlParser.hostname().c_str(), urlParser.hostname().size());
    globals.reporting_host[urlParser.hostname().size()] = 0;
    if (urlParser.port().size() == 0) {
         strcpy(globals.reporting_port, "80");
    } else {
        strncpy(globals.reporting_port, urlParser.port().c_str(), urlParser.port().size());
        globals.reporting_port[urlParser.port().size()] = 0;
    }
    // UriUriA uri;
    // const char *errorPos;
    // if (uriParseSingleUriA(&uri, options.report_url, &errorPos) != URI_SUCCESS) {
    //     printf("cannot parse reporting url: %s", options.report_url);
    //     exit(1);
    // }
    // int host_length = uri.hostText.afterLast - uri.hostText.first;
    // strncpy(globals.reporting_host, uri.hostText.first, host_length);
    // globals.reporting_host[host_length] = 0;
    // int port_length = uri.portText.afterLast - uri.portText.first;
    // strncpy(globals.reporting_port, uri.portText.first, uri.portText.afterLast - uri.portText.first);
    // globals.reporting_port[uri.portText.afterLast - uri.portText.first] = 0;
    // if (port_length == 0) {
    //     strcpy(globals.reporting_port, "80");
    // }
    // uriFreeUriMembersA(&uri);

    printf("  reporting host: %s\n", globals.reporting_host);
    printf("  reporting port: %s\n", globals.reporting_port);

    struct addrinfo hints;
    memset(&hints, 0, sizeof(hints));
    hints.ai_family = AF_UNSPEC;
    hints.ai_socktype = SOCK_STREAM;
    hints.ai_flags = 0;
    hints.ai_protocol = 0;
    hints.ai_canonname = NULL;
    hints.ai_addr = NULL;
    hints.ai_next = NULL;

    if (getaddrinfo(globals.reporting_host, globals.reporting_port, &hints, &globals.reporting_address) != 0) {
        fatal_error("getaddrinfo");
    }

    if (result == NULL) {
        fatal_error("getaddrinfo: no host found");
    }
}

int main(int argc, char* argv[]) {
    set_default_options();
    parse_options(argc, argv);

    printf("statistics monitoring proxy\n");
    if (options.verbose) {
        printf("  node name:  %s\n", options.node_name);
        printf("  listening on:  %d\n", options.listen_port);
        printf("  forwarding to: %s:%d\n", options.forward_host, options.forward_port);
        printf("  reporting for:  %s\n", options.node_name);
        printf("  verbose mode:  %s\n", options.verbose ? "true" : "false");
    }
    
    resolve_forward_address();
    resolve_reporting_domain();
    setup_listening_socket();
    signal(SIGINT, sigint_handler);
    io_uring_queue_init(QUEUE_DEPTH, &ring, 0);
    server_loop();

    return 0;
}
