#!/bin/sh
if [ -z ${NCDE_MON_LISTEN_PORT:x} ]; then 
    echo "Variable NCDE_MON_LISTEN_PORT must be set"
    exit 1
fi
if [ -z ${NCDE_MON_FORWARD_PORT:x} ]; then
    echo "Variable NCDE_MON_FORWARD_PORT must be set"
    exit 1
fi
if [ -z ${NCDE_MON_FORWARD_HOST:x} ]; then
    echo "Variable NCDE_MON_FORWARD_HOST must be set"
    exit 1
fi