/**
 * Copyright 2022 Lucerne Applied University of Sciences / IoT S&S Research Group
 *
 * Licensed under the Apache License, Version 2.0 (the "License");
 * you may not use this file except in compliance with the License.
 * You may obtain a copy of the License at
 * 
 *     http://www.apache.org/licenses/LICENSE-2.0
 *
 * Unless required by applicable law or agreed to in writing, software
 * distributed under the License is distributed on an "AS IS" BASIS,
 * WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
 * See the License for the specific language governing permissions and
 * limitations under the License.
 * 
 */

import { createStore } from 'vuex';
import elementTypes from './elementDefinitions';
import api from '../api/storage';
import validationApi from '../api/validations';
import deploymentApi from '../api/deployments';
import analysisApi from '../api/analyses';

const validateOnly = false;

const Action = Object.freeze({
    CREATED: 'CREATED',
    MODIFIED: 'MODIFIED',
    DELETED: 'DELETED'
});

const store = createStore({
    state: {
        project: null,
        events: [],
        isProcessingEvents: false,
        errors: [],
        stats: {},
        selectedElements: [],
        copiedElements: [],
        elementsUsedByMap: new Map(),         
    },
    getters: {
        hasProject(state) { return state.project != null; },
        selectedElements(state) { return state.selectedElements; },
        copiedElements(state) { return state.copiedElements; },
        canPaste(state) { 
            return state.copiedElements.length > 0
                && state.selectedElements.length == 1
                && elementTypes[state.selectedElements[0].type].children.includes(state.copiedElements[0].type + 's');
        },
        canDelete(state) { return state.selectedElements.length > 0; },     
        canCopyOrCut(state) {
            const parents = new Set();
            for (const element of state.selectedElements) {
                if (element.parent != null) {
                    parents.add(element.parent);
                }
            }
            return parents.size == 1;
        },
    },
    mutations: {
        newProject(state, payload) {
            state.project = {
                name : payload.name,
                description : payload.description,
                parent: null,
                type: 'project',
                elements: new Set(),       
                errors: new Map()
            };

            setStateCreated(state, state.project);
        },
        async loadProject(state, payload) {
            state.project = await api.getProject(payload.projectId);
            state.project.parent = null;
            state.project.elements = new Set();
            
            // for each element type get elements and load each element
            for (const [elementTypeName, elementType] of Object.entries(elementTypes)) {
                if (!elementType.isTopLevelElement) break;

                const elements = await api.getElements(state.project, elementTypeName);
                await Promise.all(elements.map(async elementInfo => {
                    let element = await api.getElement(state.project, elementInfo.id, elementTypeName);
                    convertCollectionsToSets(element);
                    element.errors = new Map();
                    addParentLink(state, element, null);
                    state.project.elements.add(element);
                }));
                updateDefinitionsAfterLoad(state, state.project);
            }
        },
        modifyProject(state, payload) {
            state.project.name = payload.name;
            state.project.description = payload.description;

            setStateModified(state, state.project);
        },
        closeProject(state) {
            state.project = null;
        },
        deleteProject(state, payload) {
            setStateDeleted(state, payload.project);
        },
        newElement(state, payload) {
            const type = payload.type;
            const parent = payload.parent;

            const element = newElement(type, parent);
            if (parent == null) {
                state.project.elements.add(element);
            } else {
                parent[type + 's'].add(element);
            }
            
            if (payload.properties) {
                for(const [key, value]  of Object.entries(payload.properties)) {
                    updateElement(state, element, key, value);
                }
            }

            if (parent != null) {
                triggerUpdate(state, parent);
            }
            
            setStateCreated(state, element);
        },
        deleteElement(state, payload) {
            const element = payload.element;
            if (element.parent == null) {
                state.project.elements.delete(element);
            } else {
                element.parent[element.type + 's'].delete(element);
                triggerUpdate(state, element.parent);
            }

            setStateDeleted(state, element);
        },
        updateElement(state, payload) {
            const element = payload.element;
            const property = payload.property;
            const value = payload.value;

            updateElement(state, element, property, value);

            setStateModified(state, element);
        },
        updatePosition(state, payload) {
            let element = payload.element;

            element.top    = payload.position.top;
            element.left   = payload.position.left;
            element.width  = payload.position.width;
            element.height = payload.position.height;
            element.index  = 1;


            triggerUpdate(state, element);

            // exclude child area
            if (element.type) {
                setStateModified(state, element);
            }
        },
        updateChildArea(state, payload) {
            let element = payload.element;
            element.childArea.top    = payload.childArea.top;
            element.childArea.left   = payload.childArea.left;
            element.childArea.width  = payload.childArea.width;
            element.childArea.height = payload.childArea.height;
        },

        updateErrors(state, payload) {
            state.errors = payload;
        },
        
        /**
         * Element selection
         */
        setSelectedElement(state, payload) {
            state.selectedElements.length = 0;
            state.selectedElements.push(payload.element);
        },
        setSelectedElements(state, payload) {
            state.selectedElements.length = 0;
            state.selectedElements.push(...payload.selectedElements);
        },
        addSelectedElement(state, payload) {
            state.selectedElements.push(payload.element);
        },
        removeSelectedElement(state, payload) {
            let index = state.selectedElements.indexOf(payload.element);
            state.selectedElements.splice(index, 1);
        },
        clearSelectedElements(state) {
            state.selectedElements.length = 0;
        },

        /**
         * Actions on selected elements
         */
        copy(state) {
            state.copiedElements = [...state.selectedElements];
        },

        paste(state) {
            for (const element of state.copiedElements) {
                const transform = {
                    scale: 1,
                    offset: { x: 0, y: 0 }
                }
                state.selectedElements[0][state.copiedElements[0].type +'s'].add(cloneElement(element, state.selectedElements[0], transform, false));
            }
        },

        cut(state) {
            state.copiedElements = [...state.selectedElements];
            for (const element of state.copiedElements) {
                const elementType = element.type + 's';
                element.parent[elementType].delete(element);
                element.parent = null;
            }
        },

        delete(state) {
            for (const element of getNonNestedElements(state.selectedElements)) {
                this.commit('deleteElement', { element: element } );
            }
            state.selectedElements = [];
        },
    },
    actions: {
        store(context) {       
            // stores all changed elements
            let events = compactEvents(context.state.events);
            const hadEvents = events.length != 0;
            while(events.length != 0) {
                let event = events.shift();
                
                if (event.element.type === 'project') {
                    switch(event.action) {
                        case Action.CREATED: { api.createProject(event.element, event.content); break; }
                        case Action.MODIFIED: { api.updateProject(event.content); break; }
                        case Action.DELETED: { api.deleteProject(event.content); break; }
                    }
                } else {
                    switch(event.action) {                        
                        case Action.CREATED: { api.createElement(context.state.project, event.element, event.content); break; }
                        case Action.MODIFIED: { api.updateElement(context.state.project, event.content); break }
                        case Action.DELETED: { api.deleteElement(context.state.project, event.content); break; }
                    }
                }
            }

            // start validation
            if (hadEvents) {
                if (validateOnly) {
                    validationApi.startValidation(context.state.project, (errors) => {
                        context.commit('updateErrors', errors);
                    });
                } else {
                    deploymentApi.startDeployment(context.state.project, (errors) => {
                        context.commit('updateErrors', errors);
                    });
                }
            }
        },
        performAnalysis(context) {
            if (context.state.project) {
                analysisApi.startAnalysis(context.state.project, (result) => {
                    context.state.stats = {};
                    for(const [node, nodeStats] of Object.entries(result.stats)) {
                        context.state.stats[node] = {};
                        for (const [measurement, value] of Object.entries(nodeStats)) {
                            context.state.stats[node][measurement] = value;
                        }
                    }
                })
            }
        }
    }
});

/* Function remove intermediate modifies, as well as transient objects
CM...MD -> (empty)
CM...M -> C with state of last modification
M....MD -> D
M...M -> M
*/
function compactEvents(events) {
    // let lastState = new Map();

    // // process events in reverse
    // for(let i = events.length - 1; i === -1; --i) {
    //     let event = events[i];
        
    //     if (event.action == Action.MODIFIED && lastState.get(element) == Action.DELETED) {
    //         // skip event
    //     } if (event.action == Action.MODIFIED && lastState.get(element) == Action.MODIFIED) {
    //         // skip event
    //     } if (event.action == Action.CREATED && lastState.get(element) == Action.DELETED) {
    //         // do noting and remove last state (update last state?)
    //     } if (event.action == Action.CREATED && lastState.get(element) == Action.MODIFIED) {
    //         // set last state of CREATED
            
    //     } else {
    //         // copy state to stack, update lastState
    //     }
    // }
    return events;
}

function newElement(type, parent) {
    const elementType = elementTypes[type];
    const element = {
        name: elementType.defaultName,
        componentName: elementType.componentName,
        parent: parent,
        type: type,
        original: null,
        errors: new Map(),
    };
    addTypeProperties(element, elementType.properties, elementType.children);
    addGetChildrenFunction(element, elementType.children);
    if (parent != null && parent.childArea) {
        addPosition(element, parent.childArea.left, parent.childArea.top, elementType.defaultWidth, elementType.defaultHeight);
    } else if (parent != null) {
        addPosition(element, parent.left - elementType.defaultWidth / 2, parent.top - elementType.defaultHeight / 2, elementType.defaultWidth, elementType.defaultHeight);
    } else {
        addPosition(element, 0, 0, elementType.defaultWidth, elementType.defaultHeight);
    }
    if (elementType.hasChildArea) {
        addChildArea(element);
    }
    return element;
}

function addTypeProperties(element, properties, childrenNames) {
    properties.forEach(property => {
        element[property] = ''
    });
    childrenNames.forEach(name => {
        element[name] = new Set();
    });
}

function addGetChildrenFunction(element, children) {
    // dynamically generate function to return all children.
    // uses eval, but is still safe as not user inputs are processed.
    let getChildrenFunction = '(function() { return [';
    let isFirst = true;
    children.forEach(name => {
        if (!isFirst) getChildrenFunction += ', ';
        getChildrenFunction += '...this.';
        getChildrenFunction += name;
        isFirst = false;
    });
    getChildrenFunction += ' ] })';
    element.getChildren = eval(getChildrenFunction);
}

function addPosition(element, left, top, width, height) {
    element.left = left;
    element.top = top;
    element.width = width;
    element.height = height;
    element.index  = 1;
}

function addChildArea(element) {
    element.childArea = {
        top:    undefined,
        left:   undefined,
        width:  undefined,
        height: undefined,
    }
}

function cloneElement(elementFrom, parent, transform, isTransisent = true) {
    const elementType = elementTypes[elementFrom.type];
    const element = {
        name: elementFrom.name,
        componentName: elementFrom.componentName,
        parent: parent,
        type: elementFrom.type,
        original: isTransisent ? elementFrom : null,
        errors: new Map(),
    };
    clonePosition(element, elementFrom, transform);
    if (elementType.hasChildArea) {
        addChildArea(element);
        clonePosition(element.childArea, elementFrom.childArea, transform);
    }
    cloneTypeProperties(element, elementFrom, transform);
    addGetChildrenFunction(element, elementType.children);
    if (elementType.children.includes('connections')) {
        updateConnections(element.connections);
    }
    return element;
}

function cloneTypeProperties(element, elementFrom, transform, isTransisent = true) {
    const elementType = elementTypes[elementFrom.type];
    // copy properties
    elementType.properties.forEach(property => {
        element[property] = elementFrom[property];
    });
    // clone children
    elementType.children.forEach(name => {
        element[name] = new Set();
        elementFrom[name].forEach(childFrom => {
            const child = cloneElement(childFrom, element, transform, isTransisent);
            element[name].add(child);
        });
    });
}

function clonePosition(element, elementFrom, transform) {
    if (['export', 'import'].includes(elementFrom.type)) {
        const parent = element.parent;
        const parentFrom = elementFrom.parent;
        const centerFromX = elementFrom.left + elementFrom.width / 2;
        const centerFromY = elementFrom.top + elementFrom.height / 2;
        const factorX = (centerFromX - parentFrom.left) / parentFrom.width;
        const factorY = (centerFromY - parentFrom.top) / parentFrom.height;
        element.left = parent.left + parent.width * factorX - elementFrom.width / 2;
        element.top = parent.top + parent.height * factorY - elementFrom.height / 2;
        element.width = elementFrom.width;
        element.height = elementFrom.height;
    } else if (transform && elementFrom.parent) {
        element.left = (elementFrom.left - elementFrom.parent.childArea.left) * transform.scale + element.parent.childArea.left;
        element.top = (elementFrom.top - elementFrom.parent.childArea.top) * transform.scale + element.parent.childArea.top;
        element.width = elementFrom.width * transform.scale;
        element.height = elementFrom.height * transform.scale;
    } else {
        element.left = elementFrom.left;
        element.top = elementFrom.top;
        element.width = elementFrom.width;
        element.height = elementFrom.height;
    }
    element.index  = elementFrom.index;
}

function findElement(state, name) {
    let foundElement = null;
    state.project.elements.forEach(element => { 
        if (element.name === name) { 
            foundElement = element
        } 
    });
    return foundElement;
}

function updateElement(state, element, property, value) {
    if (element.type === 'instance' && property === 'definition') {
        updateInstanceDefinition(state, element, value, true);
    }
    element[property] = value;
    triggerUpdate(state, element);
}

function triggerUpdate(state, element) {
    if (state.elementsUsedByMap.has(element)) {
        // create copy of instances (set is modified during loop)
        const instances = [...state.elementsUsedByMap.get(element)];
        for (const instance of instances) {
            updateInstanceDefinition(state, instance, instance.definition, true);
            triggerUpdate(state, instance);
        }
    }
}

function updateInstanceDefinition(state, instance, definition, force) {
    if (definition !== instance.definition || force) {     
        // remove existing definitions
        instance.imports = new Set();
        instance.exports = new Set();
        instance.instances = new Set();
        instance.connections = new Set();
        const oldElement = findElement(state, instance.definition);
        multisetDelete(state.elementsUsedByMap, oldElement, instance);

         // lookup new definitions
        instance.definition = definition;
        const element = findElement(state, definition);
        if (element != null) {
            // clone and link imports (always)
            element.imports.forEach(iport => {
                const imprt = cloneElement(iport, instance);
                imprt.original = null;
                instance.imports.add(imprt);
            });
            element.exports.forEach(eport => {
                const exprt = cloneElement(eport, instance);
                exprt.original = null;
                instance.exports.add(exprt);
            });

            // clone and link instances if type is a system
            if (element.type === 'system') {
                const transform = {
                    scale: Math.min(instance.childArea.width / element.childArea.width, instance.childArea.height / element.childArea.height, 1),
                    offset: {
                        x: element.childArea.left - instance.childArea.left,
                        y: element.childArea.top - instance.childArea.top
                    }
                }
                for (const connection of element.connections) {
                    const connectionClone = cloneElement(connection, instance, transform);
                    if (connectionClone.instanceFrom === instance.definition) { connectionClone.instanceFrom = instance.name; }
                    if (connectionClone.instanceTo === instance.definition) { connectionClone.instanceTo = instance.name; }
                    instance.connections.add(connectionClone);
                }

                for (const childInstance of element.instances) {
                    instance.instances.add(cloneElement(childInstance, instance, transform));
                }

                updateConnections(instance.connections);
            }

            // add link element (component/system) to instance to notify on changes
            multisetAdd(state.elementsUsedByMap, element, instance);
        }
    }
}

function getTopLevelParent(element) {
    let value = element;
    while(value.parent != null) {
        value = value.parent;
    }
    return value;
}

function setStateCreated(state, element) {
    if (element.parent === null) {
        state.events.push({ element: element, content: copy(element), action: Action.CREATED });
    } else {
        state.events.push({ element: element, content: copy(getTopLevelParent(element)), action: Action.MODIFIED });
    }
}

function setStateModified(state, element) {
    element = getTopLevelParent(element);
    if (element.id) {
        state.events.push({ element: element, content: copy(element), action: Action.MODIFIED });
    } else {
        state.events.push({ element: element, content: copy(element), action: Action.CREATED });
    }
}

function setStateDeleted(state, element) {
    if (element.parent === null) {
        state.events.push({ element: element, content: copy(element), action: Action.DELETED });
    } else {
        state.events.push({ element: element, content: copy(getTopLevelParent(element)), action: Action.MODIFIED });
    }
}


function copyPosition(element, elementFrom) {
    element.left = elementFrom.left;
    element.top = elementFrom.top;
    element.width = elementFrom.width;
    element.height = elementFrom.height;
    element.index  = elementFrom.index;
}

function copy(element) {
    const elementCopy = {};
    if (element.id !== undefined) {
        elementCopy.id = element.id;
    }
    elementCopy.name = element.name;
    elementCopy.type = element.type;

    if (element.type === 'project') {
        elementCopy.description = element.description;
    } else {
        const elementType = elementTypes[element.type];

        // copy standard elements
        copyPosition(elementCopy, element);
        if (elementType.hasChildArea && element.childArea !== undefined) {
            addChildArea(elementCopy);
            copyPosition(elementCopy.childArea, element.childArea);
        }
        
        // copy properties
        elementType.properties.forEach(property => {
            elementCopy[property] = element[property];
        });

        // copy children
        elementType.children.forEach(child => {
            elementCopy[child] = [];
            if (element[child] !== undefined) {
                element[child].forEach(childElement => {
                    if (childElement.original == null) { // no clones
                        elementCopy[child].push(copy(childElement));
                    }
                });
            }
        });
    }

    return elementCopy;
}

function addParentLink(state, element, parent) {
    const elementType = elementTypes[element.type];
    element.componentName = elementType.componentName;
    element.parent = parent;
    element.original = null,
    element.errors = new Map(),
    elementType.children.forEach(childName => {
        element[childName] = new Set(element[childName]);
        element[childName].forEach(child => {
            addParentLink(state, child, element);
        });
    });
    if (elementType.hasChildArea && !element.childArea) {
        addChildArea(element);
    }
    addGetChildrenFunction(element, elementType.children);
}

function convertCollectionsToSets(element) {
    const elementType = elementTypes[element.type];
    for (const childName of elementType.children) {
        element[childName] = new Set(element[childName]);
        for(const child of element[childName]) {
            convertCollectionsToSets(child);
        }
    }    
}

function updateDefinitionsAfterLoad(state, element) {
    switch(element.type) {
        case 'project':
            for (const child of element.elements) {
                if (['environment', 'system'].includes(child.type)) {
                    updateDefinitionsAfterLoad(state, child);
                }
            }
            break;
        case 'environment':
        case 'system':
            for (const instance of element.instances) {
                updateDefinitionsAfterLoad(state, instance);
            }
            break;
        case 'instance':
            updateInstanceDefinition(state, element, element.definition, true);
            break;
    }
}

function getNonNestedElements(elements) {
    const elementSet = new Set(elements);
    const nonNestedElements = [];
    for(const element of elementSet) {
        let parent = element.parent;
        let hasParent = false;
        while(parent != null) {
            if (elementSet.has(parent)) {
                hasParent = true;
                break;
            }                    
            parent = parent.parent;
        }
        if (!hasParent) {
            nonNestedElements.push(element);
        }
    }
    return nonNestedElements;
}

function multisetAdd(map, key, value) {
    let set;
    if (map.has(key)) {
        set = map.get(key);
    } else {
        set = new Set();
        map.set(key, set);
    }
    set.add(value);
}

function multisetDelete(map, key, value) {
    if (map.has(key)) {
        const set = map.get(key);
        set.delete(value);
    }
}

function updateConnections(elements) {
    for(const element of elements) {
        if (element.type === 'connection') {
            const connection = element;
            updateCenter(connection, connection.instanceFrom, connection.connectorFrom, 'fromX', 'fromY');
            updateCenter(connection, connection.instanceTo, connection.connectorTo, 'toX', 'toY');
            updateBoxAfterMove(connection);
        }
    }
}

function updateCenter(connection, instanceName, connectorName, propertyNameX, propertyNameY) {
    const parent = connection.parent;            
    let instance = getChild(parent, 'instance', instanceName);
    // public interfaces are on the environment level
    if (instance === null && parent.name === instanceName) {
        instance = parent;
    }
    if (instance !== null) {
        const connector = getConnectorChild(instance, connectorName);
        if (connector !== null) {
            connection[propertyNameX] = connector.left + connector.width / 2;
            connection[propertyNameY] = connector.top + connector.height / 2;
        }
    }
}

function getConnectorChild(element, name) {
    let child = null;
    for(const elementType of getConnectorElementTypes()) {
        child = getChild(element, elementType, name);
        if (child !== null) {
            break;
        }
    }
    return child;
}

function getChild(element, propertyName, name) {
    if (element[propertyName + 's']) {
        for(const child of element[propertyName + 's']) {
            if (child.name === name) {
                return child;
            }
        }
    }
    return null;
}

function updateBoxAfterMove(line) {
    line.left = Math.min(line.fromX, line.toX);
    line.top = Math.min(line.fromY, line.toY);
    line.width = Math.max(line.fromX, line.toX) - line.left;
    line.height = Math.max(line.fromY, line.toY) - line.top;
}

function getConnectorElementTypes() {
    const connectorElementTypes = [];
    for(const [key, element] of Object.entries(elementTypes)) {
        if (element.isConnector) {
            connectorElementTypes.push(key);
        }
    }
    return connectorElementTypes;
}

export default store;