<!--
  -- Copyright 2022 Lucerne Applied University of Sciences / IoT S&S Research Group
  --
  -- Licensed under the Apache License, Version 2.0 (the "License");
  -- you may not use this file except in compliance with the License.
  -- You may obtain a copy of the License at
  -- 
  --     http://www.apache.org/licenses/LICENSE-2.0
  --
  -- Unless required by applicable law or agreed to in writing, software
  -- distributed under the License is distributed on an "AS IS" BASIS,
  -- WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
  -- See the License for the specific language governing permissions and
  -- limitations under the License.
  -- 
  -->
<template>
    <div id="mainDrawArea">
    <div id="toolbar">        
        <button :disabled="!supportsAction(actions.addInstance)" @click.stop="$store.commit('newElement', {type: 'instance', parent: $store.getters.selectedElements[0]});">
            Add Instance
        </button>
        <button :disabled="!supportsAction(actions.addConnector)" @click.stop="mode = MouseMode.CONNECT; this.$store.commit('setSelectedElements', { selectedElements: [ ]})">
            Connect
        </button>
        <button :disabled="!supportsAction(actions.addImport)" @click.stop="$store.commit('newElement', {type: 'import', parent: $store.getters.selectedElements[0]});">
            Add Import
        </button>
        <button :disabled="!supportsAction(actions.addExport)" @click.stop="$store.commit('newElement', {type: 'export', parent: $store.getters.selectedElements[0]});">
            Add Export
        </button>
        <button :disabled="!supportsAction(actions.addPublicInterface)" @click.stop="$store.commit('newElement', {type: 'publicInterface', parent: $store.getters.selectedElements[0]});">
            Add Public Interface
        </button>
    </div>
    <div id="drawArea"
        @mouseup.stop="handleMouseUp($event)"
        @mousedown.stop="handleMouseDown($event, MouseMode.SELECT)"
        @mousemove.stop="handleMouseMove($event)">

        <!-- the elements of a scene -->
        <template v-for="element in allElements" v-bind:key="element">
            <component v-bind:is="element.componentName"
                :element="element"
                :transform="transform"
                :style="[getScreenPosition(element), getStyleHighlight(element)]"
                @mousedown.stop="handleMouseDown($event, MouseMode.DRAG, element)"
                @mouseup.stop="handleMouseUp($event)"
            />
        </template>
        
        <DrawConnection v-if="connectLine != null"
            :element="connectLine"
            :transform="transform"
        />

        <!-- TODO: for lines resize switches to move instead of resize -->
        <!-- the resize buttons. indicate a selected element --> 
        <div v-for="resizeButton in resizeElements"
            :key="resizeButton"
            :style="getScreenPosition(resizeButton)"
            class="sizeButton"
            :class=[resizeButton.class]
            @mousedown.stop="handleMouseDown($event, MouseMode.RESIZE, resizeButton)"
            @mouseup.stop="handleMouseUp($event)">            
        </div>

        <!-- the selection rectangle. only visible if a user is currently selecting --> 
        <div id="selector"
             :class="{hidden: !selector.visible}"
             :style="getScreenPosition(selector)">
        </div>
    </div>
    </div>
</template>

<script>
const RESIZE_BUTTON_SIZE = 10;

import DrawComponent from './ui/DrawComponent.vue';
import DrawConnection from './ui/DrawConnection.vue';
import DrawComponentInstance from './ui/DrawComponentInstance.vue';
import DrawEnvironment from './ui/DrawEnvironment.vue';
import DrawImportExport from './ui/DrawImportExport.vue';
import DrawPublicInterface from './ui/DrawPublicInterface.vue';
import DrawInterface from './ui/DrawInterface.vue';
import DrawResource from './ui/DrawResource.vue';
import DrawService from './ui/DrawService.vue';
import DrawSystem from './ui/DrawSystem.vue';

import actions from '../store/actions.js';
import elementTypes from '../store/elementDefinitions.js';

export default {
    components: { DrawComponent, DrawConnection, DrawResource, DrawSystem, DrawEnvironment, DrawInterface, DrawService, DrawComponentInstance, DrawImportExport, DrawPublicInterface },
    name: 'DrawArea',
    props: [ 'zoom', 'selectedElement' ],
    data() {
        return {
            actions: actions,
            savedEditorStates : new Map(),
            mode : 'None',
            resizeDir : { x: 0, y: 0 },
            lastCursorPosition : null,
            selector  : { visible: false, left: 0, top: 0, width: 0, height: 0 },
            transform : { top: 0, left: 0, scale: 1 },
            connectLine : null,
            highlightedElement : null,
            savedPositions : new Map(),
            MouseMode: {
                SELECT:  'Select',
                DRAG:    'Drag',
                RESIZE:  'Resize',
                CONNECT: 'Connect',
                MOVE:    'Move',
                NONE:    'None'
            },
            connectorElementTypes: this.getConnectorElementTypes(),
            resizeDirClassSuffixH: [ 'L', 'M', 'R' ],
            resizeDirClassSuffixV: [ 'T', 'M', 'B' ],
        };
    },
    watch: {
        selectedElement(newValue, oldValue) {
             this.savedEditorStates.set(oldValue, {
                 selectedElements : [...this.$store.getters.selectedElements]
             });
            const savedValues = this.savedEditorStates.get(newValue);
            if (savedValues != null) {                
                this.$store.commit('setSelectedElements', { selectedElements: savedValues.selectedElements });
            } else {
                this.$store.commit('clearSelectedElements');
            }
        },

    },
    computed: {
        allElements() {
            // get all elements of root element
            const elements = [ this.selectedElement ];          
            for(let i = 0; i < elements.length; ++i) {
                elements.push(...elements[i].getChildren());
            }

            return elements;
        },
        resizeElements() {
            const resizeButtons = [];
            this.$store.getters.selectedElements.forEach(element => {
                for(let x = -1; x < 2; ++x) {
                    for(let y = -1; y < 2; ++y) {                        
                        // no button in the middle of an element
                        if (x == 0 && y == 0) continue;
                        resizeButtons.push({
                            element: element,
                            x:       x,
                            y:       y,
                            left:    element.left + (x + 1) * element.width / 2 - RESIZE_BUTTON_SIZE / 2,
                            top:     element.top + (y + 1) * element.height / 2 - RESIZE_BUTTON_SIZE / 2,
                            width:   RESIZE_BUTTON_SIZE,
                            height:  RESIZE_BUTTON_SIZE,
                            class:   'sizeButton' + this.resizeDirClassSuffixH[x + 1] + this.resizeDirClassSuffixV[y + 1]
                        });
                    }
                }
            }, this);
            return resizeButtons;
        },
    },
    methods: {
        supportsAction(action) {
            return this.$store.getters.selectedElements.length == 1
                && this.$store.getters.selectedElements[0].original === null
                && elementTypes[this.$store.getters.selectedElements[0].type].allowedActions.has(action);
        },

        getConnectorElementTypes() {
            const connectorElementTypes = [];
            for(const [key, element] of Object.entries(elementTypes)) {
                if (element.isConnector) {
                    connectorElementTypes.push(key);
                }
            }
            return connectorElementTypes;
        },

        getOriginalState(element) {
            return this.savedPositions.get(element);
        },

        saveStates() {
            this.allElements.forEach(element => {
                this.savedPositions.set(element, this.copyElementPosition(element));
                if (element.childArea) {
                    this.savedPositions.set(element.childArea, this.copyElementPosition(element.childArea));
                }
            }, this);
        },

        restoreStates() {
            this.savedPositions.forEach((value, key) => this.setElementPosition(key, value), this);
        },

        commitState() {
            this.savedPositions.clear();
        },

        getElementsAtPosition(x, y) {
            return this.allElements.filter((element) => {
                return element.left <= x && x < (element.left + element.width) && element.top <= y && y < (element.top + element.height);
            });
        },

        getConnectorAtPosition(x, y) {
            const elements = this.getElementsAtPosition(x, y).filter((element) => {
                return this.connectorElementTypes.includes(element.type);
            });
            return elements.length > 0 ? elements[0] : null;
        },

        moveSelection(dx, dy) {   
            this.restoreStates();
            const allElements = this.expandElements(this.$store.getters.selectedElements);
            this.translateElements(allElements, dx, dy);
            //this.applyGlobalConstraints(allElements, dx, dy);
            this.applyLocalConstraints(allElements);
            this.updateConnections(this.allElements);
        },
        
        translateElements(elements, dx, dy) {
            elements.forEach(element => {
                const originalPosition = this.getOriginalState(element);
                element.left = originalPosition.left + dx;
                element.top  = originalPosition.top + dy;
                if (element.childArea != null) {
                    const originalChildAreaPosition = this.getOriginalState(element.childArea);
                    element.childArea.left = originalChildAreaPosition.left + dx;
                    element.childArea.top = originalChildAreaPosition.top + dy;
                }
            }, this);
        },

        applyGlobalConstraints(elements, dx, dy) {
            const delta = { x: 0, y: 0 };
            elements.forEach(element => {
                const localDelta = this.getGlobalConstraintViolationDelta(element);
                if (localDelta.x != 0) {
                    if (delta.x == 0 || Math.sign(localDelta.x) == Math.sign(delta.x)) {
                        delta.x = Math.sign(localDelta.x) * Math.max(Math.abs(localDelta.x), Math.abs(delta.x));
                    }
                }
                if (localDelta.y != 0) {
                    if (delta.y == 0 || Math.sign(localDelta.y) == Math.sign(delta.y)) {
                        delta.y = Math.sign(localDelta.y) * Math.max(Math.abs(localDelta.y), Math.abs(delta.y));
                    }
                }
            }, this);
            this.translateElements(elements, dx + delta.x, dy + delta.y);
        },

        getGlobalConstraintViolationDelta(element) {
            const delta = { x: 0, y: 0 }
            const parent = element.parent;
            if (parent != null && parent.childArea != null) {
                const childArea = parent.childArea;
                
                if (element.left < childArea.left) {
                    delta.x = childArea.left - element.left;
                } else if (element.left + element.width > childArea.left + childArea.width) {
                    delta.x = childArea.left + childArea.width - element.left - element.width;
                }
                
                if (element.top < childArea.top) {
                    delta.y = childArea.top - element.top;
                } else if (element.top + element.height > childArea.top + childArea.height) {
                    delta.y = childArea.top + childArea.height - element.top - element.height;
                }
            }
            return delta;
        },

        applyLocalConstraints(elements) {
            elements.forEach(element => this.applyLocalConstraintsForElement(element), this);
        },

        applyLocalConstraintsForElement(element) {
            const parent = element.parent;
            // element must not leave parent
            if (elementTypes[element.type].positionOnBorder) {
                const middleX = element.left + element.width / 2;
                const middleY = element.top + element.height / 2;
                const x1 = parent.left - middleX;
                const x2 = parent.left + parent.width - middleX;
                const y1 = parent.top - middleY;
                const y2 = parent.top + parent.height - middleY;
                const min = Math.min(Math.abs(x1), Math.abs(x2), Math.abs(y1), Math.abs(y2))
                switch(min) {
                    case Math.abs(x1):
                        element.left = parent.left - element.width / 2;
                        if (element.top < parent.top - element.height / 2) element.top = parent.top - element.height / 2;
                        if (element.top > parent.top + parent.height - element.height / 2) element.top = parent.top + parent.height - element.height / 2;
                        break;
                    case Math.abs(x2): 
                        element.left = parent.left + parent.width - element.width / 2;
                        if (element.top < parent.top - element.height / 2) element.top = parent.top - element.height / 2;
                        if (element.top > parent.top + parent.height - element.height / 2) element.top = parent.top + parent.height - element.height / 2;
                        break;
                    case Math.abs(y1):
                        element.top = parent.top - element.height / 2;
                        if (element.left < parent.left - element.width / 2) element.left = parent.left - element.width / 2;
                        if (element.left > parent.left + parent.width - element.width / 2) element.left = parent.left + parent.width - element.width / 2;
                        break;
                    case Math.abs(y2):
                        element.top = parent.top + parent.height - element.height / 2;
                        if (element.left < parent.left - element.width / 2) element.left = parent.left - element.width / 2;
                        if (element.left > parent.left + parent.width - element.width / 2) element.left = parent.left + parent.width - element.width / 2;
                        break;
                }
            }          
        },

        updateConnections(elements) {
            for(const element of elements) {
                if (element.type === 'connection') {
                    const connection = element;
                    this.updateCenter(connection, connection.instanceFrom, connection.connectorFrom, 'fromX', 'fromY');
                    this.updateCenter(connection, connection.instanceTo, connection.connectorTo, 'toX', 'toY');
                    this.updateBoxAfterMove(connection);
                }
            }
        },

        updateCenter(connection, instanceName, connectorName, propertyNameX, propertyNameY) {
            const parent = connection.parent;            
            let instance = this.getChild(parent, 'instance', instanceName);
            // public interfaces are on the environment level
            if (instance === null && parent.name === instanceName) {
                instance = parent;
            }
            if (instance !== null) {
                const connector = this.getConnectorChild(instance, connectorName);
                if (connector !== null) {
                    connection[propertyNameX] = connector.left + connector.width / 2;
                    connection[propertyNameY] = connector.top + connector.height / 2;
                }
            }
        },

        getConnectorChild(element, name) {
            let child = null;
            for(const elementType of this.connectorElementTypes) {
                child = this.getChild(element, elementType, name);
                if (child !== null) {
                    break;
                }
            }
            return child;
        },

        getChild(element, propertyName, name) {
            if (element[propertyName + 's']) {
                for(const child of element[propertyName + 's']) {
                    if (child.name === name) {
                        return child;
                    }
                }
            }
            return null;
        },

        handleMouseDown(event, mode, element) {
            const mousePosition = this.getDrawAreaPosition(event);

            if (this.mode != this.MouseMode.CONNECT) {
                this.mode = mode;
            }
            switch (this.mode) {
                case this.MouseMode.SELECT: {
                    if (!event.ctrlKey) {
                        this.$store.commit('setSelectedElements', { selectedElements: []});
                    }
                    this.updateSelector(mousePosition.x, mousePosition.y);
                    this.selector.visible = true;
                    break;
                }
                case this.MouseMode.DRAG: {
                    if (element != null && element.original === null) {
                        if (this.$store.getters.selectedElements.indexOf(element) == -1) {
                            if (event.ctrlKey) {
                                this.$store.commit('addSelectedElement', { element: element });
                            } else {
                                this.$store.commit('setSelectedElements', { selectedElements: [ element ]});
                            }
                        } else {
                            if (event.ctrlKey) {
                                this.$store.commit('removeSelectedElement', { element: element });
                            } else {
                                this.$store.commit('setSelectedElements', { selectedElements: [ element ]});
                            }
                        }
                    }
                    break;
                }
                case this.MouseMode.RESIZE: {
                    this.resizeDir = { x: element.x, y: element.y }; 
                    break;
                }
                case this.MouseMode.CONNECT: {
                    const element = this.getConnectorAtPosition(mousePosition.x, mousePosition.y);
                    if (element !== null) {
                        this.connectLine = {
                            fromX: mousePosition.x,
                            fromY: mousePosition.y,                        
                            toX: mousePosition.x,
                            toY: mousePosition.y
                        }
                        this.updateBoxAfterMove(this.connectLine);
                        this.$store.commit('setSelectedElements', { selectedElements: [ element ]});
                        this.mode = this.MouseMode.MOVE;
                    } else {
                        this.mode = this.MouseMode.NONE;
                    }
                    break;
                }
            }

            this.lastCursorPosition = mousePosition;
            this.saveStates();
        },

        handleMouseMove(event) {
            const mousePosition = this.getDrawAreaPosition(event);

            switch (this.mode) {
                case this.MouseMode.DRAG: {    
                    let dx = mousePosition.x - this.lastCursorPosition.x;
                    let dy = mousePosition.y - this.lastCursorPosition.y;
                    this.moveSelection(dx, dy);
                    break;
                }
                case this.MouseMode.SELECT: {
                    this.updateSelector(mousePosition.x, mousePosition.y);
                    break;
                }
                case this.MouseMode.RESIZE: {
                    let dx = mousePosition.x - this.lastCursorPosition.x;
                    let dy = mousePosition.y - this.lastCursorPosition.y;
                    this.resizeSelection(dx, dy);
                    break;
                }
                case this.MouseMode.MOVE: {
                    this.connectLine.toX = mousePosition.x;
                    this.connectLine.toY = mousePosition.y;
                    this.updateBoxAfterMove(this.connectLine);

                    this.highlightedElement = this.getConnectorAtPosition(mousePosition.x, mousePosition.y);
                    break;
                }
            }
        },

        handleMouseUp(event) {
            let mousePosition = this.getDrawAreaPosition(event);

            switch (this.mode) {
                case this.MouseMode.SELECT: {
                    this.updateSelectedObjects();
                    this.selector.visible = false;
                    break;
                }
                case this.MouseMode.MOVE: {
                    const element = this.getConnectorAtPosition(mousePosition.x, mousePosition.y);                    
                    this.$store.commit('newElement', { type: 'connection', parent: this.getOuterParent(this.$store.getters.selectedElements[0]), properties: {
                        'instanceFrom': this.$store.getters.selectedElements[0].parent.name,
                        'connectorFrom': this.$store.getters.selectedElements[0].name,
                        'instanceTo': element.parent.name,
                        'connectorTo': element.name,
                        'fromX': this.connectLine.fromX,
                        'fromY': this.connectLine.fromY,
                        'toX': this.connectLine.toX,
                        'toY': this.connectLine.toY
                    } });
                    this.connectLine = null;
                    this.highlightedElement = null;
                }
            }
            this.mode = this.MouseMode.NONE;
            this.lastCursorPosition = null;
            this.commitState();            
        },

        getOuterParent(element) {
            let parent = element.parent;
            while (parent !== null) {
                if (['environment', 'system'].includes(parent.type)) {
                    return parent;
                }
                parent = parent.parent;
            }
            return parent;
        },

        resizeSelection(dx, dy) {
            this.restoreStates();
            const selection = this.getSelection();
            const scale = {
                x: (selection.width  + dx * this.resizeDir.x) * this.resizeDir.x / selection.width,
                y: (selection.height + dy * this.resizeDir.y) * this.resizeDir.y / selection.height
            };
            const offset = {
                x: (this.resizeDir.x == 1 ? selection.left : 0) + (this.resizeDir.x == -1 ? selection.width  : 0),
                y: (this.resizeDir.y == 1 ? selection.top : 0) +  (this.resizeDir.y == -1 ? selection.height : 0)
            };

            // rescale each selected element
            this.resizeSelectionInner(this.$store.getters.selectedElements, offset.x, scale.x, offset.y, scale.y);

            // check parent and children constraints
            this.expandElements(this.$store.getters.selectedElements).forEach(element => this.resizeLocalConstraintsForElement(element, offset.x, scale.x, offset.y, scale.y), this);
            const scaleAdjusted = this.resizeGlobalConstraints(this.$store.getters.selectedElements, offset.x, scale.x, offset.y, scale.y);
            if (scaleAdjusted != null) {
                this.restoreStates();
                this.resizeSelectionInner(this.$store.getters.selectedElements, offset.x, scaleAdjusted.x, offset.y, scaleAdjusted.y);
            }
        },

        resizeSelectionInner(elements, baseX, scaleX, baseY, scaleY) {
            elements.forEach(function (element) {
                if (this.isResizeable(element)) {
                    this.resizeXAxis(element, baseX, scaleX);
                    this.resizeYAxis(element, baseY, scaleY);
                    if (element.childArea) {
                        this.resizeXAxis(element.childArea, baseX, scaleX);
                        this.resizeYAxis(element.childArea, baseY, scaleY);
                    }
                }
            }, this);
        },

        resizeLocalConstraintsForElement(element, baseX, scaleX, baseY, scaleY) {
            if (elementTypes[element.type].positionOnBorder) {
                const originalElement = this.getOriginalState(element);
                const middleX = originalElement.left + originalElement.width / 2;
                const middleY = originalElement.top + originalElement.height / 2;
                const scaledMiddleX = baseX + Math.abs(baseX - middleX) * scaleX;
                const scaledMiddleY = baseY + Math.abs(baseY - middleY) * scaleY;
                if (this.resizeDir.x != 0) {
                    element.left = scaledMiddleX - element.width / 2;
                }
                if (this.resizeDir.y != 0) {
                    element.top = scaledMiddleY - element.height / 2;
                }
            }
        },

        resizeGlobalConstraints(elements, baseX, scaleX, baseY, scaleY) {
            const resizedChildren = new Set();
            elements.forEach(element => {
                if (element.childArea && element.getChildren.size > 0) {
                    resizedChildren.add(...element.getChildren());
                } else {
                    if (element.parent != null && element.parent.childArea) {
                        resizedChildren.add(element);
                    }
                }
            });        
            if (resizedChildren.size > 0) {
                const scaleDelta = { x: 0, y: 0 };
                resizedChildren.forEach(element => {
                    const scale = this.getGlobalConstraintViolationScaleDelta(element, baseX, scaleX, baseY, scaleY);
                    scaleDelta.x = Math.max(scaleDelta.x, scale.x);
                    scaleDelta.y = Math.max(scaleDelta.y, scale.y);
                }, this);
                scaleDelta.x *= this.resizeDir.x;
                scaleDelta.y *= this.resizeDir.y;
                return scaleDelta;
            } else {
                return null;
            }
        },

        getGlobalConstraintViolationScaleDelta(element, baseX, scaleX, baseY, scaleY) {
            const scaleDelta = { x: Math.abs(scaleX), y: Math.abs(scaleY) };
            const originalElement = this.getOriginalState(element);
            const parent = element.parent;
            if (parent != null && parent.childArea != null) {
                const childArea = parent.childArea;
                
                if (element.left < childArea.left) {
                    scaleDelta.x = Math.abs((childArea.left - baseX) / Math.abs(baseX - originalElement.left));
                } else if (element.left + element.width > childArea.left + childArea.width) {
                    scaleDelta.x = Math.abs((childArea.left + childArea.width - baseX) / Math.abs(baseX - originalElement.left - originalElement.width));
                }
                
                if (element.top < childArea.top) {
                    scaleDelta.y = Math.abs((childArea.top - baseY) / Math.abs(baseY - originalElement.top));
                } else if (element.top + element.height > childArea.top + childArea.height) {
                    scaleDelta.y = Math.abs((childArea.top + childArea.height - baseY) / Math.abs(baseY - originalElement.top - originalElement.height));
                }
            }

            return scaleDelta;
        },
        
        updateBoxAfterMove(line) {
            line.left = Math.min(line.fromX, line.toX);
            line.top = Math.min(line.fromY, line.toY);
            line.width = Math.max(line.fromX, line.toX) - line.left;
            line.height = Math.max(line.fromY, line.toY) - line.top;
        },

        resizeXAxis(element, baseX, scaleX) {       
            const originalElement = this.getOriginalState(element);
            let right = originalElement.left + originalElement.width;
            if (this.resizeDir.x != 0) {
                element.left = baseX + Math.abs(baseX - originalElement.left) * scaleX;
                right = baseX + Math.abs(baseX - right) * scaleX;
            }
            element.width = right - element.left;
        },

        resizeYAxis(element, baseY, scaleY) {
            const originalElement = this.getOriginalState(element);
            let bottom = originalElement.top + originalElement.height;
            if (this.resizeDir.y != 0) {
                element.top = baseY + Math.abs(baseY - originalElement.top) * scaleY;
                bottom = baseY + Math.abs(baseY - bottom) * scaleY;
            }
            element.height = bottom - element.top;
        },

        isResizeable(element) {
            return elementTypes[element.type].resizeable && element.original === null;
        },
                
        getDrawAreaPosition(event) {
            const drawArea = document.getElementById('drawArea');
            const rect = drawArea.getBoundingClientRect();
            const toolbar = document.getElementById('toolbar');
            const rectToolbar = toolbar.getBoundingClientRect();
            const position = {
                x: event.clientX - rect.left,
                y: event.clientY - rect.top - rectToolbar.height
            };
            return this.screenToInternal(position);
        },
         
        getSelection() {
            let left   = Number.POSITIVE_INFINITY;
            let top    = Number.POSITIVE_INFINITY;
            let right  = Number.NEGATIVE_INFINITY;
            let bottom = Number.NEGATIVE_INFINITY;

            this.$store.getters.selectedElements.forEach(element => {
                left   = Math.min(element.left, left);
                top    = Math.min(element.top, top);
                right  = Math.max(element.left + element.width, right);
                bottom = Math.max(element.top + element.height, bottom);
            }, this);

            return {
                left:   left,
                top:    top,
                width:  right - left,
                height: bottom - top
            };
        },

        copyElementPosition(element) {
            return {
                left: element.left,
                top: element.top,
                width: element.width,
                height: element.height
            };
        },

        setElementPosition(element, position) {
            this.$store.commit('updatePosition', {
                element: element,
                position: position
            });
        },

        updateSelectedObjects() {
            this.allElements.forEach(function(element) {                
                if (this.inside(this.selector, element) && element.original === null) {                    
                    if (this.$store.getters.selectedElements.indexOf(element) == -1) {
                        this.$store.commit('addSelectedElement', { element: element});
                    } else {
                        this.$store.commit('removeSelectedElement', { element: element});
                    }
                }
            }, this);
        },

        expandElements(elements) {
            const elementStack = [...elements];
            const expandedElements = new Set();

            while(elementStack.length != 0) {
                const element = elementStack.pop();
                expandedElements.add(element);
                element.getChildren().forEach(child => elementStack.push(child));
            }

            return expandedElements;
        },

        updateSelector(x, y) {
            let left   = this.lastCursorPosition != null ? Math.min(this.lastCursorPosition.x, x) : x;
            let top    = this.lastCursorPosition != null ? Math.min(this.lastCursorPosition.y, y) : y;
            let right  = this.lastCursorPosition != null ? Math.max(this.lastCursorPosition.x, x) : x;
            let bottom = this.lastCursorPosition != null ? Math.max(this.lastCursorPosition.y, y) : y;
            this.selector.left   = left;
            this.selector.top    = top;
            this.selector.width  = right - left;
            this.selector.height = bottom - top;
        },

        inside(rect1, rect2) {
            return rect1.left <= rect2.left && rect1.left + rect1.width >= rect2.left + rect2.width
                && rect1.top  <= rect2.top  && rect1.top + rect1.height >= rect2.top + rect2.height;
        },

        screenToInternal(position) {
            return {
                x: position.x / (this.transform.scale) - this.transform.left,
                y: position.y / (this.transform.scale) - this.transform.top
            }
        },

        getScreenPosition(element) {
            return {
                left: (this.transform.left + element.left) * this.transform.scale + 'px',
                top: (this.transform.top + element.top) * this.transform.scale + 'px',
                width: element.width * this.transform.scale + 'px',
                height: element.height * this.transform.scale + 'px'
            }
        },
        getStyleHighlight(element) {
            const proeprties = {};
            if (element === this.highlightedElement) {
                proeprties['border-color'] = 'red';
            }
            return proeprties;
        },
    }
}
</script>

<style scoped>
    #mainDrawArea {
        display: grid;
        grid-template-columns: 1fr;
        grid-template-rows: 32px 1fr;
    }
    #toolbar {
        background-color: var(--bg-toolbar);
        height:32px;
    }
    .component {
        position: absolute;
        border: 1px;
        border-style: solid;
        border-color: black;
        background-color: white;
        
        z-index: 5;

        /* text properties */
        color: black;
        user-select: none;
        display: flex;
        justify-content: center;
        align-items: center;           
    }
    .sizeButton {
        border-color: #bbb;
        border-style: solid;
        border-width: 2px;
        border-radius: 50%;
        background-color:white;
        position: absolute;
        z-index: 10;
    }
    #selector {
        position: absolute;
        border: 1px;
        border-style: solid;
        border-color: blue;
        background-color: deepskyblue;
        opacity: 0.7;
        z-index: 11;
    }
    .sizeButtonLT { cursor: nwse-resize; }
    .sizeButtonMT { cursor: ns-resize;   }
    .sizeButtonRT { cursor: nesw-resize; }
    .sizeButtonLM { cursor: ew-resize;   }
    .sizeButtonRM { cursor: ew-resize;   }
    .sizeButtonLB { cursor: nesw-resize; }
    .sizeButtonMB { cursor: ns-resize;   }
    .sizeButtonRB { cursor: nwse-resize; }
    .hidden {
        display: none;
    }
    .selectedComponent {
        border: 1px;
        border-style: solid;
        border-color: black;
    }
</style>
