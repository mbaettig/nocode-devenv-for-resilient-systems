/**
 * Copyright 2022 Lucerne Applied University of Sciences / IoT S&S Research Group
 *
 * Licensed under the Apache License, Version 2.0 (the "License");
 * you may not use this file except in compliance with the License.
 * You may obtain a copy of the License at
 * 
 *     http://www.apache.org/licenses/LICENSE-2.0
 *
 * Unless required by applicable law or agreed to in writing, software
 * distributed under the License is distributed on an "AS IS" BASIS,
 * WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
 * See the License for the specific language governing permissions and
 * limitations under the License.
 * 
 */

import ajaxCallFactory from "./ajaxCall";

const apiBaseUrl = 'http://localhost:3002/';
const ajaxCall = ajaxCallFactory.create(apiBaseUrl);
const pollRate = 1000; // ms
let validationInProgress = false;

const api = {    
    async startDeployment(project, updateErrors) {
        if (validationInProgress) return;
        validationInProgress = true;
        const data = await ajaxCall('POST', 'deployments', {
            name: project.name
        });
        setTimeout(() => {
            this.getDeployment(data.id, updateErrors)
        }, pollRate);
    },

    async getDeployment(id, updateErrors) {
        try {
            const data = await ajaxCall('GET', 'deployments/' + id);
            if (data.status === 'started') {
                 setTimeout(() => { this.getDeployment(id, updateErrors) }, pollRate);
            } else if (data.status == 'done') {
                updateErrors(data.result);
                validationInProgress = false;
            } else {
                console.log('error fetching validation');
                validationInProgress = false;
            }
        } catch(error) {
            console.log('error fetching validation');
            validationInProgress = false;
        }
        },

    async cancelDeployment(id) {
        const data = await ajaxCall('DELETE', 'deployments/' + id);
        validationInProgress = false;
        return data;
    },
}

export default api;