/**
 * Copyright 2022 Lucerne Applied University of Sciences / IoT S&S Research Group
 *
 * Licensed under the Apache License, Version 2.0 (the "License");
 * you may not use this file except in compliance with the License.
 * You may obtain a copy of the License at
 * 
 *     http://www.apache.org/licenses/LICENSE-2.0
 *
 * Unless required by applicable law or agreed to in writing, software
 * distributed under the License is distributed on an "AS IS" BASIS,
 * WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
 * See the License for the specific language governing permissions and
 * limitations under the License.
 * 
 */

const ajaxCallFactory = {
    create(apiBaseUrl) {
        return async function (method, url, data) {
            const token = sessionStorage.getItem('jwt');
            if (token == null) {        
                // TODO: redirect user login screen
                throw { errorMessage: 'not logged in'};
            }

            let body = null;
            if (method == 'GET' || method == 'DELETE') {
                url += '?' + new URLSearchParams(data).toString();
            } else {
                body = JSON.stringify(data);
            }
            const response = await fetch(apiBaseUrl + url, {
                method : method,
                headers : { 
                    'Content-Type':  'application/json',
                    'Authorization': 'Bearer ' + token
                },
                body : body
            });
            const responseJson = await response.json();
            if (!response.ok) {            
                if (response.status >= 400 && response.status < 500) {
                    // user error
                    throw { errorMessage: responseJson.errorMessage };
                } else {
                    // server error
                    console.log('Error occured during ajax call: ' + response);
                    throw { errorMessage: 'internalError' };
                }
            }
            return responseJson;
        }
    }
}

export default ajaxCallFactory;