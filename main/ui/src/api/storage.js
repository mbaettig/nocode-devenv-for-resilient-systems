/**
 * Copyright 2022 Lucerne Applied University of Sciences / IoT S&S Research Group
 *
 * Licensed under the Apache License, Version 2.0 (the "License");
 * you may not use this file except in compliance with the License.
 * You may obtain a copy of the License at
 * 
 *     http://www.apache.org/licenses/LICENSE-2.0
 *
 * Unless required by applicable law or agreed to in writing, software
 * distributed under the License is distributed on an "AS IS" BASIS,
 * WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
 * See the License for the specific language governing permissions and
 * limitations under the License.
 * 
 */
import ajaxCallFactory from "./ajaxCall";

const apiBaseUrl = 'http://localhost:3000/';
const ajaxCall = ajaxCallFactory.create(apiBaseUrl);

const api = {        
    async createProject(project, contents) {
        const data = await ajaxCall('POST', 'projects', project, contents);
        project.id = data.id;
    },
    async getProjects() {
        const data = await ajaxCall('GET', 'projects', {});
        return data;
    },
    async getProject(projectId) {
        const data = await ajaxCall('GET', 'projects/' + projectId);
        return data;
    },
    async updateProject(project) {
        const data = await ajaxCall('PUT', 'projects/' + project.id, project);
        return data;
    },
    async deleteProject(project) {
        const data = await ajaxCall('DELETE', 'projects/' + project.id);
        return data;
    },
    async createElement(project, element, contents) {
        const type = element.type;
        const data = await ajaxCall('POST', 'projects/' + project.id + '/' + type + 's', contents);
        element.id = data.id;
    },
    async getElements(project, type) {
        const data = await ajaxCall('GET', 'projects/' + project.id  + '/' + type + 's');
        return data;
    },
    async getElement(project, elementId, type) {
        const data = await ajaxCall('GET', 'projects/' + project.id + '/' + type + 's/' + elementId);
        return data;
    },
    async updateElement(project, element) {
        const type = element.type;
        const data = await ajaxCall('PUT', 'projects/' + project.id  + '/' + type + 's/' + element.id, element);
        return data;
    },
    async deleteElement(project, element) {
        const type = element.type;
        const data = await ajaxCall('DELETE', 'projects/' + project.id  + '/' + type + 's/' + element.id, {});
        return data;
    }
}

export default api;