/**
 * Copyright 2022 Lucerne Applied University of Sciences / IoT S&S Research Group
 *
 * Licensed under the Apache License, Version 2.0 (the "License");
 * you may not use this file except in compliance with the License.
 * You may obtain a copy of the License at
 * 
 *     http://www.apache.org/licenses/LICENSE-2.0
 *
 * Unless required by applicable law or agreed to in writing, software
 * distributed under the License is distributed on an "AS IS" BASIS,
 * WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
 * See the License for the specific language governing permissions and
 * limitations under the License.
 * 
 */

import { createWebHistory, createRouter } from "vue-router";
import Main from "../components/Main.vue";
import LoginPage from "../components/LoginPage.vue";
import RegistrationPage from "../components/RegistrationPage.vue";
import VerificationPage from "../components/VerificationPage.vue";
import ValidateSuccessPage from "../components/ValidateSuccessPage.vue";

const routes = [
  {
    path: "/",
    name: "Main",
    component: Main,
    beforeEnter: () => {
        const jwt = sessionStorage.getItem('jwt');
        if (jwt == null) {
            router.replace('/login');
        }
    },
  },
  {
    path: "/login",
    name: "Login",
    component: LoginPage,
  },
  {
    path: "/register",
    name: "Register",
    component: RegistrationPage,
  },
  {
    path: "/validate",
    name: "Validate",
    component: VerificationPage,
  },
  {
    path: "/success",
    name: "Success",
    component: ValidateSuccessPage,
  },
];

const router = createRouter({
  history: createWebHistory(),
  routes,
});

export default router;