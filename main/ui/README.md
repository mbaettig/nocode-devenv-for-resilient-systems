# No-code Development Environment

#app.png -> system
<a href="https://www.flaticon.com/free-icons/data-center" title="data center icons">Data center icons created by Eucalyp - Flaticon</a>
#data-center.png -> resource
<a href="https://www.flaticon.com/free-icons/app" title="app icons">App icons created by Freepik - Flaticon</a>
#settings -> component
<a href="https://www.flaticon.com/free-icons/settings" title="settings icons">Settings icons created by Ilham Fitrotul Hayat - Flaticon</a>
#data icons -> environment
<a href="https://www.flaticon.com/free-icons/data" title="data icons">Data icons created by Eucalyp - Flaticon</a>
#could service -> service
<a href="https://www.flaticon.com/free-icons/cloud-service" title="cloud service icons">Cloud service icons created by kmg design - Flaticon</a>
#interface -> interface
<a href="https://www.flaticon.com/free-icons/interface" title="interface icons">Interface icons created by Freepik - Flaticon</a>