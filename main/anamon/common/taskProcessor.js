/**
 * Copyright 2022 Lucerne Applied University of Sciences / IoT S&S Research Group
 *
 * Licensed under the Apache License, Version 2.0 (the "License");
 * you may not use this file except in compliance with the License.
 * You may obtain a copy of the License at
 * 
 *     http://www.apache.org/licenses/LICENSE-2.0
 *
 * Unless required by applicable law or agreed to in writing, software
 * distributed under the License is distributed on an "AS IS" BASIS,
 * WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
 * See the License for the specific language governing permissions and
 * limitations under the License.
 * 
 */

/**
 * Task Processor API
 */
const express = require('express');
const httpJsonUtils = require('./httpJsonUtils');
const checkUser = require('./checkUser');
const { v4: uuidv4 } = require('uuid');

const TASK_STATUS = Object.freeze({
    STARTED: 'started',
    DONE: 'done',
    FAILED: 'failed',
    CANCELLED: 'cancelled'
});

function createTaskProcessor(taskName, taskProcessor) {
    const tasks = new Map();
    
    function createTask(name, userId, token) {
        const task = {
            id: uuidv4(),
            userId: userId,            
            token: token,
            status: TASK_STATUS.STARTED,
            name: name,
            result: null
        };
        tasks.set(task.id, task);
        taskProcessor(task);
        return task;
    }

    const router = express.Router();
    router.post('/', async (req, res) => {
        await httpJsonUtils.performAction(async() => {            
            httpJsonUtils.checkOrFail(req.body.name !== undefined, 400, 'Name of ' +  taskName + ' task must be defined');
            httpJsonUtils.checkOrFail(req.headers.authorization.startsWith('Bearer '), 401, "Bearer authentication token must be set");
            const token = req.headers.authorization.substring(7);
            const userId = checkUser.getUserId(token);
            httpJsonUtils.checkOrFail(userId !== null, 400, 'Token is invalid');

            console.log(req.body);
            const task = createTask(req.body.name, userId, token);
            return task;
        }, req, res, 201);
    });
    
    router.get('/:id', async (req, res) => {
        await httpJsonUtils.performAction(async() => {
            httpJsonUtils.checkOrFail(req.headers.authorization.startsWith('Bearer '), 401, "Bearer authentication token must be set");
            const token = req.headers.authorization.substring(7);
            const userId = checkUser.getUserId(token);
            httpJsonUtils.checkOrFail(userId !== null, 400, 'Token is invalid');            
            const taskId = req.params.id;
            httpJsonUtils.checkOrFail(tasks.has(taskId), 400, 'No ' +  taskName + ' task with id \'' + req.params.id + '\' found');
            const task = tasks.get(taskId);
            httpJsonUtils.checkOrFail(task.userId === userId, 403, 'No authorization to access validation');
            return task;
        }, req, res, 200);
    });
    
    router.delete('/:id', async (req, res) => {
        await httpJsonUtils.performAction(async() => {
            httpJsonUtils.checkOrFail(req.query.token !== undefined, 400, 'Token name must be defined');
    
            const taskId = parseInt(req.params.id);
            httpJsonUtils.checkOrFail(tasks.has(taskId), 400, 'No ' +  taskName + ' task with id \'' + req.params.id + '\' found');
            const task = tasks.get(taskId);
            task.status = TASK_STATUS.CANCELLED;
            return task;
        }, req, res, 200);
    });    

    return router;
}

module.exports = createTaskProcessor;
