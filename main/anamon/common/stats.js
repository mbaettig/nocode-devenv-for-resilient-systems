const stats = {
    stats: {},
    update(node, statistic, value) {
        let nodeStats = this.stats[node];
        if (nodeStats === undefined) {
            nodeStats = {};
            this.stats[node] = nodeStats;
        }
        nodeStats[statistic] = value;
    },
    getStats() {
        return this.stats;
    }
}

module.exports = stats;