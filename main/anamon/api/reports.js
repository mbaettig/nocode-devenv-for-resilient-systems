/**
 * Copyright 2022 Lucerne Applied University of Sciences / IoT S&S Research Group
 *
 * Licensed under the Apache License, Version 2.0 (the "License");
 * you may not use this file except in compliance with the License.
 * You may obtain a copy of the License at
 * 
 *     http://www.apache.org/licenses/LICENSE-2.0
 *
 * Unless required by applicable law or agreed to in writing, software
 * distributed under the License is distributed on an "AS IS" BASIS,
 * WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
 * See the License for the specific language governing permissions and
 * limitations under the License.
 * 
 */

const express = require('express');
const stats = require('../common/stats');

const router = express.Router();
router.get('/stats/:node', async (req, res) => {
    await httpJsonUtils.performAction(async() => {
        httpJsonUtils.checkOrFail(req.params.node !== undefined, 400, 'Node must be specified');
        httpJsonUtils.checkOrFail(req.query.bytes_in !== undefined, 400, 'bytes_in must be specified');
        httpJsonUtils.checkOrFail(req.query.bytes_out !== undefined, 400, 'bytes_out must be specified');
        httpJsonUtils.checkOrFail(req.query.requests !== undefined, 400, 'requests must be specified');

        stats.update(req.params.node, 'bytesIn', req.query.bytes_in);
        stats.update(req.params.node, 'bytesOut', req.query.bytes_out);
        stats.update(req.params.node, 'requests', req.query.bytes_out);
    }, req, res, 200);
});

module.exports = router;
