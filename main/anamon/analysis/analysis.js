/**
 * Copyright 2022 Lucerne Applied University of Sciences / IoT S&S Research Group
 *
 * Licensed under the Apache License, Version 2.0 (the "License");
 * you may not use this file except in compliance with the License.
 * You may obtain a copy of the License at
 * 
 *     http://www.apache.org/licenses/LICENSE-2.0
 *
 * Unless required by applicable law or agreed to in writing, software
 * distributed under the License is distributed on an "AS IS" BASIS,
 * WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
 * See the License for the specific language governing permissions and
 * limitations under the License.
 * 
 */

/**
 * Note: Availability is calculated for each public interface and instance using basic probability theory:
 * - Each node can fail
 * - Each resource can fail
 * The availability of a single node is the availability of the node instance multiplied by the avaibility of the resource.
 * -> If a node is replicated N times, we use the formula 1-(1 - availbility)^N.
 * -> The current version does not respect the fact, that load balancers may fail as well.
 */
const loadProject = require('../common/loadProject');
const { assert } = require('node:console');
const multiSetFactory = require('../common/multiset');
const stats = require('../common/stats');

// a load balaner is a special case that has a listening port, but multiple connections
const LOAD_BALANCER_IMAGE = 'ncde-loadbalancer:latest';
const LOAD_BALANCER_FRONTEND_PORT = 'HAPROXY_FRONTEND_PORT';
const LOAD_BALANCER_BACKEND_HOST = 'HAPROXY_BACKENDS';
const LOAD_BALANCER_BACKEND_PORT = 'HAPROXY_BACKEND_PORTS';

// a storage replicator is a special load balancer that forwards write 
// requests to all hosts but read only from one host
const REPLICATION_IMAGE = 'ncde-replication:latest';
const REPLICATION_FRONTEND_PORT = 'HAREPL_FRONTEND_PORT';
const REPLICATION_BACKEND_HOST = 'HAREPL_BACKEND_HOSTS';
const REPLICATION_BACKEND_POST = 'HAREPL_BACKEND_PORTS';

// monitoring is forwarder with statistics collection (think of socat)
const MONITORING_IMAGE = 'ncde-monitoring:lastest';
const MONITORING_ENV_LISTEN_PORT = 'NCDE_MON_LISTEN_PORT';
const MONITORING_ENV_HOST = 'NCDE_MON_FORWARD_HOST';
const MONITORING_ENV_PORT = 'NCDE_MON_FORWARD_PORT';

const projectDeploymentFactory = {
     create() {
         return {
             project: null,
             context: contextFactory.create(),
             nodes: new Map(),
             edges: new Set(),
             edgesFrom: multiSetFactory.create(),
             edgesTo: multiSetFactory.create(),
             async analyze(task) {
                console.log('perform analysis' + task.id);
                const project = await loadProject(task.name, task.token);
                this.project = project;
                this.createNodesFromProject(project);
                this.insertManualLoadBalancers();
                this.insertScaling();
                this.calculateAvailability();
                console.log('analysis done ' + task.id);
                return stats;
             },
             createNodesFromProject(project) {  
                 console.log('createNodesFromProject(' + project.name + ')');
 
                 for (const element of project.elements) {
                     if (element.type === 'environment') {
                         this.createsNodeFromEnvironment(element);
                     }
                 }
             },
 
             createsNodeFromEnvironment(environment) {                
                 console.log('createsNodeFromEnvironment(' + environment.name + ')');

                 this.context.addLevel(environment);
                 this.context.setResourceMappings(this.getResourceMappings(environment));
                 for (const instance of environment.instances) {
                     if (instance.definitionType === 'system') {
                         this.createNodesFromSystem(environment, instance);
                     } else if (instance.definitionType == 'component') {
                         this.createNodesFromComponent(environment, instance);
                     }
                 }
                 for (publicInterface of environment.publicInterfaces) {
                     this.createNodesFromPublicInterface(environment, publicInterface);
                 }
                 this.context.removeLevel();
             },
 
             createNodesFromSystem(parent, systemInstance) {
                 console.log('createNodesFromSystem(' + systemInstance.name + ')');
                 this.context.addLevel(systemInstance);
 
                 const systemDefinition = this.getElement(systemInstance.definition, systemInstance.definitionType);
                 for (const instance of systemDefinition.instances) {
                     if (instance.definitionType === 'system') {
                         this.createNodesFromSystem(systemInstance, instance);
                     } else if (instance.definitionType === 'component') {
                         this.createNodesFromComponent(systemInstance, instance);
                     }
                 }
                 this.context.removeLevel();
             },
 
             createNodesFromComponent(parent, instance) {
                 console.log('createNodesFromInstance(' + instance.name + ')');
                 const network = this.context.getCurrentLevel();
                 
                 const nodeFullName = makeFullName(this.context.getCurrentLevel(), instance.name, 'nd', 1);
                 const node = this.getOrCreateNode(nodeFullName);
                 node.context = this.context.getCurrentLevel();
                 node.name = instance.name;
                 node.type = 'nd';
                 node.sequence = 1;
                 const componentDefinition = this.getElement(instance.definition, instance.definitionType);                
                 node.availability = componentDefinition.availability;
                 node.networks.push(network);
                 for (const environmentVariable of componentDefinition.environmentVariables) {
                     node.environment[environmentVariable.name] = environmentVariable.value;
                 }
                 
                 for (const connection of this.getConnections(parent, instance)) {
                     const parents = this.context.getLevels();
                     // get actual target instance
                     const res = this.getTargetComponentInstance(connection, parents);
                     const targetDefinition = this.getElement(res.targetInstance.definition, res.targetInstance.definitionType);
                     const hasTargetNode = res.targetInstance.definitionType !== 'service';
                     const targetNodeFullName = makeFullName(res.context, res.targetInstance.name, 'nd', 1);
                     const targetNode = hasTargetNode ? this.getOrCreateNode(targetNodeFullName) : null;
                     if (hasTargetNode) {
                         const edge = edgeFactory.create();                        
                         edge.from = node;
                         edge.to = targetNode;
                         const imprt = this.getChildElement(componentDefinition, 'import', connection.connectorFrom);
                         const exprt = this.getChildElement(targetDefinition, 'export', res.connectorTo);
                         edge.connectorFrom = connection.connectorFrom; // keep to identify insertion points for load balancing
                         edge.environmentFromHost = imprt.environmentHost;
                         edge.environmentFromPort = imprt.environmentPort;
                         edge.environmentToPort = exprt.environmentPort;
                         this.addEdge(edge);
                     }
                 }
             },
 
             createNodesFromPublicInterface(parent, publicInterface) {
                 console.log('createNodesFromPublicInterface(' + publicInterface.name + ')');
                 const network = this.context.getCurrentLevel();
                 const nodeFullName = makeFullName(this.context.getCurrentLevel(), publicInterface.name, 'pi', 1);
                 const node = this.getOrCreateNode(nodeFullName);
                 node.context = this.context.getCurrentLevel();
                 node.name = publicInterface.name;
                 node.type = 'pi';
                 node.sequence = 1;
                 node.publicPort = publicInterface.port;
                 node.availability = 1;
                 node.networks.push(network);
 
                 for (const connection of this.getConnections(parent, parent)) {
                     const parents = this.context.getLevels();
                     // get actual target instance
                     const res = this.getTargetComponentInstance(connection, parents);
                     const targetDefinition = this.getElement(res.targetInstance.definition, res.targetInstance.definitionType);
                     const hasTargetNode = res.targetInstance.definitionType !== 'service';
                     const targetNodeFullName = makeFullName(res.context, res.targetInstance.name, 'nd', 1);
                     const targetNode = hasTargetNode ? this.getOrCreateNode(targetNodeFullName) : null;
                     if (hasTargetNode) {
                         const edge = edgeFactory.create();
                         edge.from = node;
                         edge.to = targetNode;
                         const exprt = this.getChildElement(targetDefinition, 'export', res.connectorTo);
                         edge.connectorFrom = connection.connectorFrom; // keep to identify insertion points for load balancing
                         edge.environmentFromHost = null;
                         edge.environmentFromPort = null;
                         edge.environmentToPort = exprt.environmentPort;
                         this.addEdge(edge);
                     }
                 }
             },
 
             getConnections(parent, instance) {
                 let connections;
                 if (parent.type == 'environment') {
                     connections = parent.connections;
                 } else if (parent.type === 'instance' && parent.definitionType === 'system') {
                     const system = this.getElement(parent.definition, parent.definitionType);
                     connections = system.connections;
                 } else {
                     assert(false, 'getConnections(' + parent.type + ', ' + instance.definitionType + ')');
                 }
                 return connections.filter((connection) => {
                     return connection.instanceFrom == instance.name
                 });
             },
 
             getResourceMappings(environment) {
                 const resourceMappings = new Map();
                 resourceMappings.set(environment.name, this.expandResourceString(environment.resource));
                 for (const resourceAssignment of environment.resourceAssignments) {                    
                     resourceMappings.set(environment.name + resourceAssignment.name.replace('/', '.'), this.expandResourceString(resourceAssignment.value));
                 }
                 return resourceMappings;
             },
 
             expandResourceString(resourceString) {
                 const resourceMapping = [];                
                 const resources = resourceString.split(',');
                 for (const resource of resources) {
                     const info = resource.split(';');
                     resourceMapping.push({
                         resource: this.getElement(info[0], 'resource'),
                         scale: info[1]
                     });
                 }
                 return resourceMapping;
             },
 
             /**
              * Follows a connection chain until its final component. The idea is that
              * starting at an import of a component, the function will find an outer system
              * if there is one, then it will find the target system(s) if there are any,
              * and finally returns the target component instance.
              * 
              * Using an EBNF-like notation, a valid connection chain is formally described as:
              * importComponent-> (importSystem->)* (exportSystem->)* exportComponent
              * 
              * Note: The chain can be entered at any point.
              */
             getTargetComponentInstance(connection, parents) {
                 console.log('getTargetComponentInstance');
                 const parent = parents[parents.length - 1];
                 const parentDefinition = (parent.type === 'instance') ? this.getElement(parent.definition, parent.definitionType) : parent;                
                 const targetInstance = this.getChildElementNullAllowed(parentDefinition, 'instance', connection.instanceTo);
                 if (targetInstance !== null) {
                     if (targetInstance.definitionType === 'component') {
                         let prefix = null;
                         for (const parent of parents) {
                             prefix = (prefix === null) ? parent.name : (prefix + '.' + parent.name);
                         }
                         return {
                             targetInstance: targetInstance,
                             connectorTo: connection.connectorTo,
                             context: prefix
                         }
                     } else {
                         const system = this.getElement(targetInstance.definition, targetInstance.definitionType);
                         const nextConnection = system.connections.filter((c) => {
                             return c.instanceFrom == system.name && c.connectorFrom === connection.connectorTo;
                         });
                         parents.push(targetInstance);
                         return this.getTargetComponentInstance(nextConnection[0], parents);
                     }
                 } else if (targetInstance === null && connection.instanceTo === parent.definition) {   
                     parents.pop();
                     const greatParent = parents[parents.length - 1]; // must exist
                     let connections = null;
                     if (greatParent.type === 'environment') {
                         connections = greatParent.connections;
                     } else {
                         const parentSystem = this.getElement(greatParent.definition, greatParent.definitionType);
                         connections = parentSystem.connections;
                     }
                     const nextConnection = connections.filter((c) => {
                         return c.instanceFrom == parent.name && c.connectorFrom === connection.connectorTo;
                     });
 
                     return this.getTargetComponentInstance(nextConnection[0], parents);
                 }
             },
 
             insertManualLoadBalancers() {     
                 console.log('insertManualLoadBalancers');
                 for (const node of [...this.nodes.values()]) {
                     const mapping = new Map();                    
                     for (const edge of this.edgesFrom.get(node) ?? []) {
                         if (mapping.has(edge.connectorFrom)) {
                             mapping.get(edge.connectorFrom).push(edge);
                         } else {
                             mapping.set(edge.connectorFrom, [ edge ]);
                         }
                     }
                     
                     // remove all mappings that do not require load balancing
                     for (const [key, value] of mapping.entries()) {
                         if (value.length <= 1) {
                             mapping.delete(key);
                         }
                     }
                     
                     // add load balancing nodes and connect them
                     for (const edges of [...mapping.values()]) {
                         const nodeId = makeFullName(node.context, node.name + '.split', 'lbm', 1);
                         const lbNode = this.getOrCreateNode(nodeId);
                         lbNode.context = node.context;
                         lbNode.name = node.name + '.split'; // note: determine resource assignment
                         lbNode.type = 'lbm';
                         lbNode.sequence = 1;
                         lbNode.availability = 1;
                         lbNode.networks = [...node.networks];
 
                         // create edge to load balancer
                         const edgeToLb = edgeFactory.create();
                         edgeToLb.from = edges[0].from;
                         edgeToLb.to = lbNode;
                         edgeToLb.connectorFrom = 'generated';
                         edgeToLb.environmentFromHost = edges[0].environmentFromHost;
                         edgeToLb.environmentFromPort = edges[0].environmentFromPort;
                         edgeToLb.environmentToPort = LOAD_BALANCER_FRONTEND_PORT;
                         this.addEdge(edgeToLb);
                         for (const edge of edges) {
                             const edgeFromLb = edgeFactory.create();
                             edgeFromLb.from = lbNode;
                             edgeFromLb.to = edge.to;
                             edgeFromLb.connectorFrom = 'generated';
                             edgeFromLb.environmentFromHost = LOAD_BALANCER_BACKEND_HOST;
                             edgeFromLb.environmentFromPort = LOAD_BALANCER_BACKEND_PORT;
                             edgeFromLb.environmentToPort = edge.environmentToPort;
                             this.addEdge(edgeFromLb);
                         }
                         for (const edge of edges) {
                             this.deleteEdge(edge);
                         }
                     }
                 }
             },
 
             insertScaling() {
                 const lbMapping = new Map();
                 const basisNodes = [...this.nodes.values()];
                 const replicatedNodes = multiSetFactory.create();
 
                 // PHASE 1: For each node with scaling > 1: Create load balancer and replicated nodes
                 for (const node of basisNodes) {
                     if (node.type === 'pi') continue;
 
                     // scale node and insert lb (currently only on first resource)
                     const resourceMappings = this.context.getResourceMapping(node.context + '.' + node.name);
                     const resourceLists = [];
                     for (const resourceMapping of resourceMappings) {
                         for (let i = 0; i < resourceMapping.scale; ++i) {
                             resourceLists.push(resourceMapping.resource);
                         }
                     }
                     node.resource = resourceLists[0]; // assign resource to node
 
                     if (resourceLists.length <= 1) continue; // only proceed for mappings > 1
                     
                     // create load balancing node
                     const lbNodeName = makeFullName(node.context, node.name, 'lba', 1);
                     const lbNode = this.getOrCreateNode(lbNodeName);
                     lbNode.context = node.context;
                     lbNode.name = node.name;
                     lbNode.type = 'lba';
                     lbNode.sequence = 1;
                     lbNode.availability = 1;
                     lbNode.networks = [...node.networks];
                     lbNode.resource = resourceLists[0];
                     lbMapping.set(node, lbNode);
 
                     // replicate nodes
                     for (let i = 1; i < resourceLists.length; ++i) {
                         const fullNodeName = makeFullName(node.context, node.name, node.type, i + 1);
                         const replicatedNode = this.getOrCreateNode(fullNodeName);
                         replicatedNode.context = node.context;
                         replicatedNode.name = node.name;
                         replicatedNode.type = node.type;
                         replicatedNode.sequence = i + 1;
                         replicatedNode.availability = node.availability;
                         replicatedNode.networks = [...node.networks];
                         replicatedNode.environment = {...node.environment};
                         replicatedNode.resource = resourceLists[i];
                         replicatedNodes.add(node, replicatedNode);
                     }
 
                     // for all nodes including original node: add edge from load balancer
                     // to node for each incomming edge of original node
                     const edgesTo = [...this.edgesTo.get(node) ?? []];
                     for (const scaledNode of [ node, ...replicatedNodes.get(node) ]) {
                         for (const edge of edgesTo) {
                             const newEdge = edgeFactory.create();
                             newEdge.from = lbNode;
                             newEdge.to = scaledNode;
                             newEdge.environmentFromHost = LOAD_BALANCER_BACKEND_HOST;
                             newEdge.environmentFromPort = LOAD_BALANCER_BACKEND_PORT;
                             newEdge.environmentToPort = edge.environmentToPort;
                             this.addEdge(newEdge);
                         }
                     }
                 }
 
                 // PHASE 2: copy outgoing edges
                 for(const basisNode of replicatedNodes.map.keys()) {
                     for(const replicatedNode of replicatedNodes.get(basisNode)) {
                         for (const edge of [...this.edgesFrom.get(basisNode) ?? []]) {
                             const newEdge = edgeFactory.create();
                             newEdge.from = replicatedNode;
                             newEdge.to = edge.to;
                             newEdge.environmentFromHost = edge.environmentFromHost;
                             newEdge.environmentFromPort = edge.environmentFromPort;
                             newEdge.environmentToPort = edge.environmentToPort;
                             this.addEdge(newEdge);
                         }
                     } 
                 }
 
                 // PHASE 3: redirect to load balancer nodes
                 for (const edge of this.edges) {
                     if (lbMapping.has(edge.to) && edge.from.type != 'lba') {
                         const newEdge = edgeFactory.create();
                         newEdge.from = edge.from;
                         newEdge.to = lbMapping.get(edge.to);
                         newEdge.environmentFromHost = edge.environmentFromHost;
                         newEdge.environmentFromPort = edge.environmentFromPort;
                         newEdge.environmentToPort = LOAD_BALANCER_FRONTEND_PORT;
                         this.addEdge(newEdge);
                         this.deleteEdge(edge);
                     }
                 }
             },
            
             calculateAvailability() {
                this.availabilityMapping = new Map();
                for (const node of this.nodes.values()) {
                    this.calculateAvailabilityForNode(node);
                }      
                for (const node of this.nodes.values()) {
                    if (node.type === 'nd' || node.type === 'pi') {
                        stats.update(node.context + '.' + node.name, 'availability', this.availabilityMapping.get(node));
                    }
                }
             },
             availabilityMapping: new Map(),
             calculateAvailabilityForNode(node) {
                // if availability is already calculated for a node return it
                if (this.availabilityMapping.has(node)) {
                    return this.availabilityMapping.get(node);
                }

                // for each outgoing edge get availability
                const availabilities = [];                
                for (edge of this.edgesFrom.get(node) ?? []) {
                    availabilities.push(this.calculateAvailabilityForNode(edge.to));
                }

                // get availability of component
                let result = node.availability;
                if (node.type !== 'pi') {
                    result *= node.resource.availability;
                }
                // get availability of resource
                if (node.type === 'lba' || node.type == 'lbm') {
                    let inverse;
                    for (const availability of availabilities) {
                        if (!inverse) {
                            inverse = 1 - availability;
                        } else {
                            inverse *= 1 - availability;
                        }
                    }
                    result = 1 - inverse;
                } else {
                    for (const availability of availabilities) {
                        result *= availability;
                    }
                }
                this.availabilityMapping.set(node, result);
                return result;
             },
  
             getOrCreateNode(id) {
                 if (this.nodes.has(id)) {
                     return this.nodes.get(id);
                 } else {
                     const node = nodeFactory.create();
                     this.nodes.set(id, node);
                     return node;
                 }                
             },
 
             addNode(name, node) {
                 this.nodes.set(name, node);
             },
 
             addEdge(edge) {
                 this.edges.add(edge);
                 this.edgesFrom.add(edge.from, edge);
                 this.edgesTo.add(edge.to, edge);
             },
 
             deleteEdge(edge) {
                 this.edges.delete(edge);
                 this.edgesFrom.delete(edge.from, edge);
                 this.edgesTo.delete(edge.to, edge);
             },
 
 
             getElement(name, type) {
                 const foundElements = this.project.elements.filter((element) => {
                     return element.name === name && element.type == type;
                 });
                 if (foundElements.length == 0) {
                     throw { message: `no element ${name} of type '${type}' found` };
                 } else if (foundElements.length > 1) {
                     throw { message: `more than one element with '${name}' and type '${type}' found` };
                 } else {
                     return foundElements[0];
                 }
             },
 
             getChildElement(parent, type, name) {
                 const foundElements = parent[type + 's'].filter((element) => {
                     return element.name === name;
                 });
                 if (foundElements.length == 0) {
                     throw { message: `no element ${name} of type '${type}' found` };
                 } else if (foundElements.length > 1) {
                     throw { message: `more than one element with '${name}' and type '${type}' found` };
                 } else {
                     return foundElements[0];
                 }
             },
 
             getChildElementNullAllowed(parent, type, name) {
                 const foundElements = parent[type + 's'].filter((element) => {
                     return element.name === name;
                 });
                 if (foundElements.length == 0) {
                     return null;
                 } else if (foundElements.length > 1) {
                     throw { message: `more than one element with '${name}' and type '${type}' found` };
                 } else {
                     return foundElements[0];
                 }
             },

         }
     }
 }
 
 const contextFactory = {
     create() {
         return {
             levels: [],
             resourceMappings: null,
             addLevel(element) {
                 this.levels.push(element);
             },
             getCurrentLevel() {
                 let value = null;
                 for (const level of this.levels) {
                     value = (value === null) ? level.name : (value + '.' + level.name);
                 }
                 return value;
             },
             makeLevel(depth) {
                 let value = null;
                 for (let i = 0; i < depth; ++i) {
                     value = (value === null) ? this.levels[i].name : (value + '.' + this.levels[i].name);
                 }
                 return value;
             },
             getLevels() {
                 return [...this.levels];
             },
             removeLevel() {
                 return this.levels.pop();
             },
             setResourceMappings(resourceMappings) {
                 this.resourceMappings = resourceMappings;
             },
             getResourceMapping(nodeName) {
                 const levels = nodeName.split('.')                
                 for (let i = levels.length - 1; i >= 0; --i) {
                     const level = levels.slice(0, i + 1).join('.');
                     if (this.resourceMappings.has(level)) {
                         return this.resourceMappings.get(level);
                     }
                 }
                 return [];
             }
         }
     }
 }
 
 const nodeFactory = {
     create() {        
         return {
             context: null,
             name: null,
             type: null,
             sequence: 1,
             image: null,
             networks: [],
             environment: {},
             resource: null,
             getNodeName() {
                 return makeFullName(this.context, this.name, this.type, this.sequence);
             },
             toString() {
                 let result = '\n\n';
                 result += 'node: '      + this.getNodeName() + '\n';
                 result += ' -> availability: '   + this.availability + '\n';
                 result += ' -> resource: ' + this.resource + '\n';
                 result += ' -> environment: ' + '\n';
                 for (const [key, value] of Object.entries[this.environment] ?? []) {
                     result += '     -> ' + key + ': ' + value + '\n';
                 }
                 result += ' -> networks:';
                 for (const network of this.networks) {
                     result += ' ' + network;
                 }
                 return result;
             }
         }
     }
 }
 
 const edgeFactory = {
     create() {        
         return {
             from: null,
             to: null,
             connectorFrom: null,
             environmentFromHost: null,
             environmentFromPort: null,
             environmentToPort: null,
             port: null,
             toString() {
                 let result = '\n\n';
                 result += 'edge:\n';
                 result += ' -> from:'     + this.from.getNodeName() + '\n';
                 result += ' -> to: '   + this.to.getNodeName() + '\n';
                 result += ' -> connectorFrom: ' + this.connectorFrom + '\n';
                 result += ' -> port: ' + this.port + '\n';
                 result += ' -> environmentFromHost: ' + this.environmentFromHost + '\n';
                 result += ' -> environmentFromPort: ' + this.environmentFromPort + '\n';
                 result += ' -> environmentToPort: ' + this.environmentToPort + '\n';
                 return result;
             }
         }
     }
 }
 
 function makeFullName(context, instanceName, type, sequence) {
     return context + '.' + instanceName + '.' + type + sequence.toString().padStart(4, '0');
 }

 module.exports = projectDeploymentFactory;
 