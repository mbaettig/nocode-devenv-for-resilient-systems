/**
 * Copyright 2022 Lucerne Applied University of Sciences / IoT S&S Research Group
 *
 * Licensed under the Apache License, Version 2.0 (the "License");
 * you may not use this file except in compliance with the License.
 * You may obtain a copy of the License at
 * 
 *     http://www.apache.org/licenses/LICENSE-2.0
 *
 * Unless required by applicable law or agreed to in writing, software
 * distributed under the License is distributed on an "AS IS" BASIS,
 * WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
 * See the License for the specific language governing permissions and
 * limitations under the License.
 * 
 */
const http = require('http');
var cors = require('cors');
const createError = require('http-errors');
const express = require('express');
const logger = require('morgan');
const environment = require('./common/environment');

// setup app express
const app = express();
app.set('port', environment.getListenPort());
app.options('*', cors());
app.use(logger('dev'));
app.use(express.json());
app.use(express.urlencoded({ extended: false }));
app.use(function (req, res, next) {
    res.header("Access-Control-Allow-Origin", "*");
    res.header("Access-Control-Allow-Headers", "Origin, X-Requested-With, Content-Type, Accept");
    next();
});

// register controllers
app.use('/analyses', require('./api/analysis'));
app.use('/reports', require('./api/reports'));

// catch 404 and forward to error handler
app.use(function (req, res, next) {
    next(createError(404));
});

// standard error handler
app.use(function (err, req, res, next) {
    res.locals.message = err.message;
    res.locals.error = environment.showErrors() ? err : {};

    // render the error page
    res.status(err.status || 500);
    res.write('<h1>Error</h1><br><pre>' + err.message + '</pre>');
    res.end('');
});

/**
 * Create HTTP server and listen on provided port and network interfaces.
 */
const server = http.createServer(app);
server.listen(environment.getListenPort());
server.on('error', onError);
server.on('listening', onListening);

/**
 * HTTP server error event: handle common listen errors with friendly messages.
 */
function onError(error) {
    if (error.syscall === 'listen' && error.code === 'EACCES') {
        console.error('Port ' + environment.getListenPort() + ' requires elevated privileges');
        process.exit(1);

    } else if (error.syscall === 'listen' && error.code === 'EADDRINUSE') {
        console.error('Port ' + environment.getListenPort() + ' is already in use');
        process.exit(1);

    } else {
        throw error;
    }
}

/**
 * HTTP listening event: provide informative output.
 */
function onListening() {
    console.log('Analysis and monitoring service listening on port ' + server.address().port);
}
