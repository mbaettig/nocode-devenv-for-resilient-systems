#!/bin/sh

if [ -z ${LISTEN_PORT} ]; then
    echo "Variable LISTEN_PORT must be set"
    exit 1
fi

exec mongod --port ${LISTEN_PORT}


