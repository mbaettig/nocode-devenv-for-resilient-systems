<!DOCTYPE html>
<html>
    <head>
        <title>Einladung zur Abstimmung</title>
        <meta charset="utf8">
    </head>
    <body>
    
        <h2>Neuen Benutzer einladen</h2>
        <form action="index.php" method="POST">
            <label for="email">Email:</label>
            <input type="text" name="email">
            <input type="submit" value="Einladen">
        </form>

    <?php 
    use PHPMailer\PHPMailer\PHPMailer;
    use PHPMailer\PHPMailer\SMTP;
    use PHPMailer\PHPMailer\Exception;

    //Load Composer's autoloader
    require 'PHPMailer/PHPMailer.php';
    require 'PHPMailer/SMTP.php';

    function myMail($email, $subject, $body) {
        $mail = new PHPMailer(true);

        try {
	    echo 'sending to ' . getenv('MAIL_HOST') . ':' . getenv('MAIL_PORT') . '...';
            $mail->isSMTP();
            $mail->Host = getenv('MAIL_HOST');
            $mail->SMTPAuth = false; 
            $mail->Port = getenv('MAIL_PORT');
        
            //Recipients
            $mail->setFrom('votes@example.com', 'Votes Mailer');
            $mail->addAddress($email); 

            //Content
            $mail->isHTML(true);
            $mail->Subject = $subject;
            $mail->Body    = $body;
            $mail->send();
            echo 'Message has been sent';
        } catch (Exception $e) {
            echo "Message could not be sent. Mailer Error: {$mail->ErrorInfo}";
        }
    }

    if (isset($_POST['email'])) {
        // Erstelle zufälliges Token
        $token = bin2hex(random_bytes(24));

        // Speicherung des Eintrags in der Tabelle VOTE
	$conn = mysqli_connect(getenv("DB_HOST"), getenv("MARIADB_USER"), getenv("MARIADB_PASSWORD"), "invited_vote", getenv("DB_PORT"));
        $stmt = mysqli_prepare($conn, "insert into vote (email, token) values (?, ?)");
        mysqli_stmt_bind_param($stmt, 'ss', $_POST['email'], $token);

        if (mysqli_stmt_execute($stmt)) {
            // Sende Einladungsmail (ggf. URL an Speicherort von vote_forn anpassen)
            $message = "Please use the following link to vote:\r\n"
                     . "http://localhost/invited_vote/vote_form.php?email=" . $_POST['email'] . "&token=" . $token. "\r\n";

            myMail($_POST['email'], "Einladung zum Abstimmen", $message);

        } else {
            // Fehlerfall: Benachrichtige Benutzer
            echo "<p>" . $_POST['email'] . " konnte nicht hinzugefuegt werden!</p>";
        }

        mysqli_close($conn);
    }?>

    </body>
</html>
